#delcare user variables
USER_DEFINE=
APPNAME=ereport
#
PRG_SOURCE_PATH=.\source
C_SOURCE_PATH=.\source
CPP_SOURCE_PATH=.\source
OBJ_PATH=.\obj
RES_PATH=.\resources
HBDIR=C:\Harbour\harbvc20142906
HBLIB=$(HBDIR)\lib
FWDIR=c:\fwh\fwh1408
FWINC=$(FWDIR)\include
FWLIB=$(FWDIR)\lib
LD=link
CC=cl
LD_FLAGS=/NOLOGO /SUBSYSTEM:WINDOWS /FORCE:MULTIPLE /NODEFAULTLIB:libc /MACHINE:X86 /OUT:$(APPNAME).exe
CC_FLAGS=-nologo -c -TP -W3 -GA -I$(HBDIR)\include -I$(FWDIR)\include -I.\include -I$(VCDIR)\include
RC=rc

#should use tabs to indent lines
#we dont need extention
SRC_FILES_PRG=ereport epfunc erfile eritems eritems2 ertools vrd ermain vrdbcode vrditem strings tcfoldex
SRC_FILES_C=cfunc treeview setmask
SRC_FILES_CPP=
#resource
RC_FILES=ereport

#seting libs without extention and path
GUI_LIBS=fivehc32 fiveh32

HRB_LIB=hbrtl hbvmmt gtgui gtwin hblang hbmacro hbrdd rddntx rddcdx rddfpt hbsix hbdebug\
        hbcommon hbpp hbwin hbcpage hbct xhb hbpcre png hbzlib hbmzip hbziparc minizip 

MSVC_LIB=kernel32 user32 gdi32 winspool comctl32 comdlg32 advapi32 shell32 ole32 oleaut32\
         uuid odbc32 odbccp32 iphlpapi mpr version wsock32 psapi msimg32 libcmt oldnames libcpmt oledlg ws2_32 winmm
#user lib
#USER_LIBS=

#the libraries of msvc dont need this process
LIBS = $(addprefix $(FWLIB))\
       $(addsuffix .lib,$(GUI_LIBS))\
       $(addprefix $(HBLIB))\
       $(addsuffix .lib,$(HRB_LIB))\
       $(addsuffix .lib,$(MSVC_LIB))\
       $(USER_LIBS)

OBJ_FILES = $(addprefix $(OBJ_PATH)\,$(notdir $(addsuffix .obj,$(SRC_FILES_PRG))))\
            $(addprefix $(OBJ_PATH)\,$(notdir $(addsuffix .obj,$(SRC_FILES_C))))\
            $(addprefix $(OBJ_PATH)\,$(notdir $(addsuffix .obj,$(SRC_FILES_CPP))))

RES_FILES = $(addprefix $(RES_PATH)\,$(notdir $(addsuffix .res,$(RC_FILES))))

LD_CMD=$(LD) $(OBJ_FILES) $(RES_FILES) $(LIBS) $(LD_FLAGS) > link.log
PRG_COMP_CMD=$(HBDIR)\bin\harbour $< /q0 /n /W /O$(OBJ_PATH)\ /i$(FWDIR)\include;$(HBDIR)\include;.\include $(USER_DEFINE)

define BUILD_APP
    @echo $(LD_CMD) >> make.log
    @$(LD_CMD)
endef

define HAR_CMD
    @echo $(PRG_COMP_CMD) $< >> make.log
    @$(PRG_COMP_CMD) >> make.log
endef

.PONNY: app clean

app : logfile $(APPNAME)

#is extremely necessary ident with tab inside targer-pattern
#should use tabs to indent lines
logfile:
    @echo. > make.log
    @echo # ----------------------------------------------------------------------- >> make.log
    @echo # Building $(APPNAME)                                                     >> make.log
    @echo # ----------------------------------------------------------------------- >> make.log
    $(info ----------------------------------------------------------------------- )
    $(info Building $(APPNAME)                                                     )
    $(info ----------------------------------------------------------------------- )


$(APPNAME): $(OBJ_FILES) $(RES_FILES)
    $(info Linking $(APPNAME) )
    @$(BUILD_APP)

$(OBJ_PATH)%.c: $(PRG_SOURCE_PATH)%.prg
    $(info Compiling $< )
    @$(HAR_CMD)

$(OBJ_PATH)%.obj: $(OBJ_PATH)%.c
    @echo $(CC) $(CC_FLAGS) -Fo$@ $< >> make.log
    @$(CC) $(CC_FLAGS) -Fo$@ $< >> make.log
    @echo ========================================= >> make.log

$(RES_PATH)%.res: $(RES_PATH)%.rc
    $(info Compiling $< )
    @echo $(RC) -r -d__FLAT__ $< >> make.log
    @$(RC) -r -d__FLAT__ $< >> make.log

$(OBJ_PATH)%.obj: $(C_SOURCE_PATH)%.c
    @echo $(CC) $(CC_FLAGS) -Fo$@ $<  >> make.log
    @$(CC) $(CC_FLAGS) -Fo$@ $< >> make.log
    $(info Compiling $< )

$(OBJ_PATH)%.obj: $(CPP_SOURCE_PATH)%.cpp
    @echo $(CC) $(CC_FLAGS) -Fo$@ $<  >> make.log
    @$(CC) $(CC_FLAGS) -Fo$@ $< >> make.log
    $(info Compiling $< )
    

clean:
    @if EXIST obj\*.* del /F /Q obj\*.*
    @if EXIST lib\*.* del /F /Q lib\*.*
    @if EXIST *.bak del /F /Q *.bak
    @if EXIST *.log del /F /Q *.log
    @if EXIST res\*.res del /F /Q res\*.res
    
 