#Microsoft VS2013 make sample, (c) FiveTech Software 2014

HBDIR=C:\Harbour\harbvc20142906
FWDIR=c:\Fwh\fwh1408
VCDIR="c:\Program Files (x86)\Microsoft Visual Studio 12.0\VC"
SDKDIR="c:\Program Files (x86)\Windows Kits\8.1"

PRGS = \
.\ereport.prg \
.\epfunc.prg \
.\erfile.prg \
.\eritems.prg \
.\eritems2.prg \
.\ertools.prg \
.\vrd.prg \
.\ermain.prg \
.\vrdbcode.prg \
.\vrditem.prg \
.\strings.prg \
.\tcfoldex.prg

C = \
.\cfunc.c \
.\treeview.c \
.\setmask.c

.SUFFIXES: .prg .c .obj .rc .res

OBJ=$(PRGS:.prg=.obj)
OBJS=$(OBJ:.\=.\obj\)

COBJ=$(C:.c=.obj)
COBJS=$(COBJ:.\=.\obj\)

CFILES=$(C:.\=.\source\)

ereport.exe : $(OBJS) $(COBJS) ereport.res
   echo $(OBJS) > msvc.tmp
   echo $(COBJS) >> msvc.tmp
   echo $(FWDIR)\lib\FiveH32.lib $(FWDIR)\lib\FiveHC32.lib >> msvc.tmp
   echo $(HBDIR)\lib\hbrtl.lib    >> msvc.tmp
   echo $(HBDIR)\lib\hbvm.lib     >> msvc.tmp
   echo $(HBDIR)\lib\gtgui.lib    >> msvc.tmp
   echo $(HBDIR)\lib\hblang.lib   >> msvc.tmp
   echo $(HBDIR)\lib\hbmacro.lib  >> msvc.tmp
   echo $(HBDIR)\lib\hbrdd.lib    >> msvc.tmp
   echo $(HBDIR)\lib\rddntx.lib   >> msvc.tmp
   echo $(HBDIR)\lib\rddcdx.lib   >> msvc.tmp
   echo $(HBDIR)\lib\rddfpt.lib   >> msvc.tmp
   echo $(HBDIR)\lib\hbsix.lib    >> msvc.tmp
   echo $(HBDIR)\lib\hbdebug.lib  >> msvc.tmp
   echo $(HBDIR)\lib\hbcommon.lib >> msvc.tmp
   echo $(HBDIR)\lib\hbpp.lib     >> msvc.tmp
   echo $(HBDIR)\lib\hbwin.lib    >> msvc.tmp
   echo $(HBDIR)\lib\hbcplr.lib   >> msvc.tmp
   echo $(HBDIR)\lib\xhb.lib      >> msvc.tmp
   echo $(HBDIR)\lib\hbpcre.lib   >> msvc.tmp
   echo $(HBDIR)\lib\hbct.lib     >> msvc.tmp
   echo $(HBDIR)\lib\hbcpage.lib  >> msvc.tmp
   echo $(HBDIR)\lib\hbzlib.lib   >> msvc.tmp
   echo $(HBDIR)\lib\png.lib      >> msvc.tmp

   echo kernel32.lib  >> msvc.tmp
   echo user32.lib    >> msvc.tmp
   echo gdi32.lib     >> msvc.tmp
   echo winspool.lib  >> msvc.tmp
   echo comctl32.lib  >> msvc.tmp
   echo comdlg32.lib  >> msvc.tmp
   echo advapi32.lib  >> msvc.tmp
   echo shell32.lib   >> msvc.tmp
   echo ole32.lib     >> msvc.tmp
   echo oleaut32.lib  >> msvc.tmp
   echo uuid.lib      >> msvc.tmp
   echo odbc32.lib    >> msvc.tmp
   echo odbccp32.lib  >> msvc.tmp
   echo iphlpapi.lib  >> msvc.tmp
   echo mpr.lib       >> msvc.tmp
   echo version.lib   >> msvc.tmp
   echo wsock32.lib   >> msvc.tmp
   echo msimg32.lib   >> msvc.tmp
   echo oledlg.lib    >> msvc.tmp
   echo psapi.lib     >> msvc.tmp
   echo gdiplus.lib   >> msvc.tmp
   echo winmm.lib     >> msvc.tmp

   IF EXIST ereport.res echo ereport.res >> msvc.tmp
   
   link @msvc.tmp /nologo /subsystem:windows /NODEFAULTLIB:msvcrt > link.log
   @type link.log
#   @del $(PRGS:.prg=.obj)

$(PRGS:.prg=.obj) : $(PRGS:.prg=.c)
$(PRGS:.prg=.c) : $(PRGS)

ereport.res : ereport.rc
   rc.exe -r -d__FLAT__ ereport.rc 

.prg.c:
   $(HBDIR)\bin\harbour $< /n /i$(FWDIR)\include;$(HBDIR)\include
   
.c.obj:
   cl.exe -c -TC -W3 -I$(HBDIR)\include -I$(SDKDIR)\include -I$(VCDIR)\include $<   

.\obj\cfunc.obj : .\source\cfunc.c
   cl.exe -c -TC -W3 -I$(HBDIR)\include -I$(SDKDIR)\include -I$(VCDIR)\include -Fo.\obj\cfunc.obj .\source\cfunc.c 

.\obj\setmask.obj : .\source\setmask.c
   cl.exe -c -TC -W3 -I$(HBDIR)\include -I$(SDKDIR)\include -I$(VCDIR)\include -Fo.\obj\setmask.obj .\source\setmask.c 

.\obj\treeview.obj : .\source\treeview.c
   cl.exe -c -TC -W3 -I$(HBDIR)\include -I$(SDKDIR)\include -I$(VCDIR)\include -Fo.\obj\treeview.obj   .\source\treeview.c 
 