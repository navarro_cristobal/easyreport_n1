
/*
 * $Id: XCBROWSE.prg 78 2013-05-22 21:46:24Z Cnl $
 */

/*
 *
 * Copyright 2013 Cristobal Navarro Lopez <navarro.cristobal / at / gmail.com>
 * www - http://
 *
 * Este programa no es de libre distribuci�n, ni puede ser usado sin
 * el expl�cito permiso del autor.
 *
 * Clase para realizar MENUs y "mejorar" visualizacion basado en XBROWSE
 *
 * CLASS TXCBrowse FROM TXBrowse
 *
 */


#include "FiveWin.ch"
#include "InKey.ch"
#include "constant.ch"
#include "xbrowse.ch"
#include "Report.ch"
#include "dtpicker.ch"
#include "hbcompat.ch"

#include "colores.ch"

#xtranslate MinMax( <xValue>, <nMin>, <nMax> ) => ;
   Min( Max( <xValue>, <nMin> ), <nMax> )

#ifdef __HARBOUR__
   #ifndef __XHARBOUR__
      #xtranslate DbSkipper => __DbSkipper
   #endif
#endif

#define SRCCOPY      0x00CC0020

#define GWL_STYLE             -16
#define GWL_EXSTYLE           -20   // 2009-11-11

#define GW_HWNDFIRST            0
#define GW_HWNDNEXT             2

#define SM_CYVSCROLL            20
#define SM_CYHSCROLL             3

#define CS_DBLCLKS              8

#define COLOR_SCROLLBAR         0
#define COLOR_BACKGROUND        1
#define COLOR_ACTIVECAPTION     2
#define COLOR_INACTIVECAPTION   3
#define COLOR_MENU              4
#define COLOR_WINDOW            5
#define COLOR_WINDOWFRAME       6
#define COLOR_MENUTEXT          7
#define COLOR_WINDOWTEXT        8
#define COLOR_CAPTIONTEXT       9
#define COLOR_ACTIVEBORDER      10
#define COLOR_INACTIVEBORDER    11
#define COLOR_APPWORKSPACE      12
#define COLOR_HIGHLIGHT         13
#define COLOR_HIGHLIGHTTEXT     14
#define COLOR_BTNFACE           15
#define COLOR_BTNSHADOW         16
#define COLOR_GRAYTEXT          17
#define COLOR_BTNTEXT           18
#define COLOR_INACTIVECAPTIONTEXT 19
#define COLOR_BTNHIGHLIGHT      20

#define DT_TOP                      0x00000000
#define DT_LEFT                     0x00000000
#define DT_CENTER                   0x00000001
#define DT_RIGHT                    0x00000002
#define DT_VCENTER                  0x00000004
#define DT_BOTTOM                   0x00000008
#define DT_WORDBREAK                0x00000010
#define DT_SINGLELINE               0x00000020
#define DT_EXPANDTABS               0x00000040
#define DT_TABSTOP                  0x00000080
#define DT_NOCLIP                   0x00000100
#define DT_EXTERNALLEADING          0x00000200
#define DT_CALCRECT                 0x00000400
#define DT_NOPREFIX                 0x00000800
#define DT_INTERNAL                 0x00001000
#define DT_EDITCONTROL              0x00002000
#define DT_PATH_ELLIPSIS            0x00004000
#define DT_END_ELLIPSIS             0x00008000
#define DT_MODIFYSTRING             0x00010000
#define DT_RTLREADING               0x00020000
#define DT_WORD_ELLIPSIS            0x00040000
#define DT_NOFULLWIDTHCHARBREAK     0x00080000
#define DT_HIDEPREFIX               0x00100000

#define MK_MBUTTON          0x0010

#define COL_EXTRAWIDTH        6
#define ROW_EXTRAHEIGHT       4
#define COL_SEPARATOR         2  //2
#define BMP_EXTRAWIDTH        5

#define RECORDSELECTOR_WIDTH 25

#define BITMAP_HANDLE         1
#define BITMAP_PALETTE        2
#define BITMAP_WIDTH          3
#define BITMAP_HEIGHT         4
#define BITMAP_ZEROCLR        5
#define BITMAP_ALPHA          6

#define VSCROLL_MAXVALUE      10000  // never set values above 32767

#define TME_LEAVE             2
#define WM_MOUSELEAVE       675
#define PM_REMOVE        0x0001

#define WM_NCMOUSEMOVE                  0x00A0


/*
#ifdef __XHARBOUR__
   #xtranslate hb_hKeyAt( <h>, <n> )     => hGetKeyAt( <h>, <n> )
   #xtranslate hb_hValueAt( <h>, <n> )   => hGetValueAt( <h>, <n> )
   #xtranslate hb_hCaseMatch( <h>, <n> ) => hSetCaseMatch( <h>, <n> )
   #xtranslate hb_WildMatch( <a>, <b> [, <c> ] ) => WildMatch( <a>, <b> [, <c> ] )
   #xtranslate HB_STRTOHEX( <c> ) => STRTOHEX( <c> )
   #xtranslate HB_HEXTOSTR( <c> ) => HEXTOSTR( <c> )
#endif

#ifndef __XHARBOUR__
   #xtranslate \<|[<x,...>]| => {|<x>|
   #xcommand > [<*x*>]       => } <x>
#endif
*/

/*

  METHOD ADDColumn en el XBrowse original le falta CLASS XBROWSE en el c�digo: influye en algo?


*/

#xcommand @ <nRow>, <nCol> [ COLUMN ] XCBROWSE  <oBrw>  ;
               [ [ FIELDS ] <Flds,...>] ;
               [ <dsrc: ALIAS, ARRAY, RECSET, RECORDSET, OBJECT, DATASOURCE> <uDataSrc> ] ;
               [ <sizes:FIELDSIZES, SIZES, COLSIZES> <aColSizes,...> ] ;
               [ <head:HEAD,HEADER,HEADERS> <aHeaders,...> ] ;
               [ <pic: PICS, PICTURE, PICTURES> <aPics,...> ] ;
               [ <cols: COLS, COLUMNS> <aCols,...> ] ;
               [ <idx: SORT,ORDERS> <aSort,...> ] ;
               [ JUSTIFY <aJust,...> ] ;
               [ SIZE <nWidth>, <nHeigth> ] ;
               [ ID <nID> ] ;
               [ <dlg:OF,DIALOG> <oWnd> ] ;
               [ SELECT <cField> FOR <uValue1> [ TO <uValue2> ] ] ;
               [ <change: ON CHANGE, ON CLICK> <uChange> ] ;
               [ ON [ LEFT ] DBLCLICK <uLDblClick> ] ;
               [ ON RIGHT CLICK <uRClick> ] ;
               [ FONT <oFont> ] ;
               [ CURSOR <oCursor> ] ;
               [ <color: COLOR, COLORS> <nClrFore> [,<nClrBack>] ] ;
               [ MESSAGE <cMsg> ] ;
               [ <update: UPDATE> ] ;
               [ <pixel: PIXEL> ] ;
               [ WHEN <uWhen> ] ;
               [ <design: DESIGN> ] ;
               [ VALID <uValid> ] ;
               [ <autosort: AUTOSORT> ] ;
               [ <autocols: AUTOCOLS> ] ;
               [ <footers: FOOTERS> ] ;
               [ <fasted: FASTEDIT> ] ;
               [ <lcell: CELL> ] [ <llines: LINES> ] ;
               [ ROWS <aRows> ] ;
               [ BACKGROUND <uBack> [ <bckmode: TILED, STRETCH, FILL, VERTICAL, HORIZONTAL> ] ] ;
               [ CLASS <child> ] [ <transp: TRANSPARENT> ] [ <noborder: NOBORDER> ] ;
      => ;
          <oBrw> := XCbrowseNew( <oWnd>, <nRow>, <nCol>, <nWidth>, <nHeigth>,;
                           [ \{ <{Flds}> \} ], ;
                           [\{<aHeaders>\}], [\{<aColSizes>\}], ;
                           [<{uChange}>],;
                           [\{|nRow,nCol,nFlags|<uLDblClick>\}],;
                           [\{|nRow,nCol,nFlags|<uRClick>\}],;
                           <oFont>, <oCursor>, <nClrFore>, <nClrBack>, <cMsg>,;
                           <.update.>, <uDataSrc>, <{uWhen}>,;
                           <.design.>, <{uValid}>, <.pixel.>, [<nID>], <.autosort.>, <.autocols.> , ;
                           [\{<aPics>\}], [\{<aCols>\}],;
                           [\{<aJust>\}], [\{<aSort>\}], <.footers.>, <.fasted.>, ;
                           <.lcell.>, <.llines.>, <aRows>, <uBack>, [upper(<(bckmode)>)], ;
                           [ If( ValType( <child> ) == 'B', <child>, <{child}> ) ], <.transp.>, <.noborder.> )

static lFoco
static bXBrowse
//----------------------------------------------------------------------------//
MEMVAR nRefresh

CLASS TXCBrowse FROM TXBrowse

   CLASSDATA lRegistered

   DATA lBorder
   DATA nWRecordSel     // Ancho de la columna de Selector de Registro: A�adida por Cnl -> 16/02/2005
   DATA nSpRecordSel    // Espacios que desplazo el bitmap de Selector de Reg. a la derecha: A�adida por Cnl -> 16/02/2005
   DATA lLinDataSep     // Linea de Separacion del Selector con los datos: A�adida por Cnl -> 16/02/2005
   DATA lLinHeadVSep    // Linea de Separacion del Selector con Cabecera: A�adida por Cnl -> 16/02/2005
   DATA lLinHeadHSep    // Linea de Separacion del Selector con Cabecera: A�adida por Cnl -> 16/02/2005
   DATA lLinFootVSep    // Linea de Separacion del Selector con Cabecera: A�adida por Cnl -> 16/02/2005
   DATA lLinFootHSep    // Linea de Separacion del Selector con Cabecera: A�adida por Cnl -> 16/02/2005
   DATA lLinBorder      // Linea de Definici�n del Control: A�adida por Cnl -> 01/03/2005
   DATA lHeadBtton      // Bordes superiores redondeados del header
   DATA lHeadSelec      // El color del Header empieza en la 1� columna y no en la del RecordSelector
   DATA lRowDividerComplete // 17/05/2013
   DATA lNoEmpty        // Salta las celdas vacias hasta la siguiente no vacia
   DATA lOneCol
   //DATA bMMoved
   /*
   DATA nRecSelWidth AS NUMERIC INIT RECORDSELECTOR_WIDTH   // FWH 11.06
     // Relacionarla con la DATA que yo añadí nWRecordSel

   */
   DATA lMultiHeight
   DATA aHeightRow
   DATA bCalcHeightRow
   DATA lWheelH

   METHOD New( oWnd )
   //METHOD End()                               INLINE ::Destroy()
   METHOD Paint()
   METHOD DrawLine( lSelected, nRowSel )
   //METHOD LButtonDown( nRow, nCol, nFlags )
   //METHOD RButtonDown( nRow, nCol, nKeyFlags )
   METHOD KeyChar( nKey, nFlags )
   METHOD GetDlgCode( nLastKey )
   METHOD KeyDown( nKey, nFlags )
   METHOD MouseMove( nRow, nCol, nKeyFlags )

   //METHOD GoDown( nDown )
   //METHOD GoUp( nUp )
   //METHOD AddCol()
   METHOD AddColumn()
   METHOD PaintHeader( hDC, aCols, nLast, hWhitePen, hGrayPen, hColPen )
   METHOD PaintFooter( hDC, nBrwWidth, nBrwHeight, hWhitePen, hGrayPen )
   METHOD GetDisplayCols()
   METHOD SetColumns( nRow, nCol, nFlags )
   //METHOD HandleEvent( nMsg, nWParam, nLParam )

   /*Eval( ::OnMouseMove, Self, nRow, nCol, nKeyFlags )*/
   METHOD ColNoEmpty()
   METHOD RowNoEmpty()

   METHOD MouseWheel( nKey, nDelta, nXPos, nYPos )
   METHOD SetTree( oTree, aResource, bOnSkip, aCols )

ENDCLASS

//----------------------------------------------------------------------------//

METHOD New( oWnd ) CLASS TXCBrowse

   ::Super:New( oWnd )
   // TXCBrwColumn
   ::bColClass        := { || TXCBrwColumn() }
   //::l2007            := .F.

   ::lBorder          := .F.
   ::nWRecordSel      := RECORDSELECTOR_WIDTH   //25
   ::nSpRecordSel     := 0
   ::lLinDataSep      := .t.
   ::lLinHeadVSep     := .t.
   ::lLinHeadHSep     := .t.
   ::lLinFootVSep     := .t.
   ::lLinFootHSep     := .t.

   ::lLinBorder       := .t.
   ::lHeadBtton       := .f.
   ::lHeadSelec       := .t.

   ::nRecSelWidth     := ::nWRecordSel
   ::lRowDividerComplete := .F.
   ::lNoEmpty         := .F.

   ::lMultiHeight     := .F.
   ::aHeightRow       := {}
   ::bCalcHeightRow   := { | n | ::aHeightRow[ n ] }
   ::lWheelH          := .F.

return Self

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//

METHOD Paint() CLASS TXCBrowse

   local aCols
   local aColors
   local oRect
   local oCol
   local nFor
   local nLen
   local nRow
   local nCol, nHeight, nLast, nTemp, nTemp2
   local nBrwWidth, nBrwHeight, nWidth
   local hBrush, hDC, hGrayPen, hWhitePen, hColPen, hRowPen, hSelBrush
   local nFirstRow, nLastRow, nMaxRows, nRowStyle, nColStyle
   local nRowPos, nRowHeight, nBookMark, nMarqStyle, nScan
   local lRecSel, lOnlyData, lHighLite
   local aInfo
   local lFinDatos     := .F.
   local hBrushSel
   local aColorsSel    := {}

   //if ::nRecSelWidth < RECORDSELECTOR_WIDTH
   //   ::nRecSelWidth    := RECORDSELECTOR_WIDTH
   //endif

   if ::SelectedCol():oEditLbx != nil
      return nil
   endif

   if !::lHeader
      ::nHeaderHeight   := 0
   else
      if !Empty( ::nHeaderHeight )
         ::lHeader      := .T.
      endif
   endif

   if !::lFooter
      ::nFooterHeight   := 0
   else
      if !Empty( ::nFooterHeight )
         ::lFooter      := .T.
      endif
   endif


   aInfo := ::DispBegin()
   ::CheckSize()

   // Paint Background
   if ::lTransparent .and. Empty( ::oBrush:hBitmap )
      if ! Empty( ::oWnd:oBrush:hBitmap )
         SetBrushOrgEx( ::hDC, nBmpWidth( ::oWnd:oBrush:hBitmap ) - ::nLeft, ;
                               nBmpHeight( ::oWnd:oBrush:hBitmap ) - ::nTop )
      endif
      FillRect( ::hDC, GetClientRect( ::hWnd ), ::oWnd:oBrush:hBrush )
   else
      oRect       := ::DataRect()
      SetBrushOrgEx( ::hDC, oRect:nLeft, oRect:nTop )
      FillRect( ::hDC, oRect:aRect, ::oBrush:hBrush )
   endif

   // Paint Background end

   while ! ::IsDisplayPosVisible( ::nColSel ) .and. ::nColSel > 1
      ::nColSel--
      ::nColOffSet++
      ::GetDisplayCols()
   end

   nLen       := Len( ::aDisplay )
   aCols      := Array( nLen + 1)
   nBrwWidth  := ::BrwWidth()
   nBrwHeight := ::BrwHeight() + if( empty( ::cCaption ), 0 , ::nRowHeight )
   nRow       := 0
   nCol       := 0
   nFirstRow  := 0
   nLastRow   := nBrwHeight
   nLast      := ::LastDisplayPos()
   nMarqStyle := ::nMarqueeStyle
   nRowStyle  := ::nRowDividerStyle
   nColStyle  := ::nColDividerStyle
   hDC        := ::hDC
   hGrayPen   := ::hBtnShadowPen
   hWhitePen  := ::hWhitePen
   hColPen    := ::hColPen
   hRowPen    := ::hRowPen
   lRecSel    := ::lRecordSelector
   lOnlyData  := ::lRefreshOnlyData
   lHighLite  := .f.

   /*
   Rowselector
   */

   if lRecSel
      nCol += ::nRecSelWidth
      if !lOnlyData
         //  FillRect( hDC, {0, 0, nLastRow + 3, nCol - 1}, ::hBrushRecSel )
         FillRect( hDC, {0, 0, nLastRow + 3, nCol }, ::hBrushRecSel ) // Fills one pixel gap between recsel and data area

         //aColorsSel := Eval( ::aCols[1]:bClrHeader, )
         //hBrushSel  := CreateSolidBrush( aColorsSel[ 2 ] )
         //FillRect( hDC, { 0, 0, nLastRow + 3, nCol }, hBrushSel )
         DeleteObject( hBrushSel )

         nHeight := ::HeaderHeight()
         nTemp   := nBrwHeight - ::FooterHeight() + 3
         if ::lLinDataSep     // A�adida por Cnl: 16/02/2005
            DrawVert( hDC, nCol - 3, nHeight, nTemp,          hWhitePen )
            DrawVert( hDC, nCol - 2, nHeight, nTemp,          hGrayPen )
            DrawVert( hDC, 0,        0,       nBrwHeight + 3, hGrayPen )
            DrawVert( hDC, 1,        0,       nBrwHeight + 3, hWhitePen )
         endif
      endif

      //nCol --
   endif

   for nFor := 1 to nLast
      aCols[ nFor ] := nCol
      oCol  := ::ColAtPos( nFor )
      nCol  += oCol:nWidth + COL_SEPARATOR
   next

   aCols[ nFor ] := nCol

   // Paint Header

   if ::lHeader
      if !lOnlyData
         ::PaintHeader( hDC, aCols, nLast, hWhitePen, hGrayPen, hColPen )
      endif
      nFirstRow += ::nHeaderHeight
   endif

   // Paint Footer
   if ::lFooter
      if !lOnlyData
         ::PaintFooter( hDC, aCols, nLast, nBrwWidth, nBrwHeight, hWhitePen, hGrayPen )
      endif
      nLastRow -= ::nFooterHeight
   endif

   /*
   Paint cols data
   */

      ::lRefreshOnlyData := .f.

      if ::nLen == 0 .and. ( !::lRowDividerComplete  .or. !::lColDividerComplete )
         ::EraseData( nFirstRow  )
         ::DispEnd( aInfo )
         return nil
      endif

      nRowHeight := ::nRowHeight
      nHeight    := ::DataHeight() // nRowHeight - 2
      nMaxRows   := ::RowCount()
      nRowPos    := 1
      nRow       := nFirstRow
      nBookMark  := Eval( ::bBookMark )

   //   Eval( ::bSkip, 1 - Min( ::nRowSel, nMaxRows ) )
      if ::nLen > 0
         ::Skip( 1 - Min( ::nRowSel, nMaxRows ) )
      endif

      if nMarqStyle > MARQSTYLE_HIGHLCELL // .and. aCols[ nLast + 1 ] < nBrwWidth
         if ::hWnd == GetFocus()
            hSelBrush := CreateColorBrush( Eval( If( ::bClrRowFocus == nil, ::bClrSelFocus, ::bClrRowFocus ) )[ 2 ] )
         else
            hSelBrush := CreateColorBrush( Eval( ::bClrSel )[ 2 ] )
         endif
      //else
      //   hSelBrush := CreateColorBrush( Eval( ::bClrSel )[ 2 ] )
      endif

      do while nRowPos <= nMaxRows

         // We must also paint some times after the last visible column

         if hSelBrush != nil

            lHighLite := ::lMultiSelect .and. ( Ascan( ::aSelected, Eval( ::bBookMark ) ) > 0 )

//          lHighLite := ( nMarqStyle == MARQSTYLE_HIGHLROWMS .or. ::nMarqueeStyle == MARQSTYLE_HIGHLWIN7 ) .and. ;
//                       ( Ascan( ::aSelected, Eval( ::bBookMark ) ) > 0 )

            if aCols[ nLast + 1 ] < nBrwWidth
               nTemp     := nRow + nHeight
               nTemp2    := aCols[nLast + 1]
               if nColStyle < LINESTYLE_INSET
                  nTemp2--
               endif              //::nMarqueeStyle
               if lHighLite .and. nMarqStyle != MARQSTYLE_HIGHLWIN7
                  FillRect( hDC, {nRow, nTemp2, nTemp, nBrwWidth }, hSelBrush )
               else
                 if nMarqStyle == MARQSTYLE_HIGHLROWMS
                   if ! ::lTransparent
                      hBrush := CreateColorBrush( Eval( ::bClrStd )[ 2 ] )
                      FillRect( hDC, {nRow, nTemp2, nTemp, nBrwWidth }, hBrush )
                      DeleteObject( hBrush )
                   else
                      // �?
                   endif
                 else
                   // �?
                 endif
               endif
            endif
         endif

         for nFor := 1 to nLast
            if aCols[ nFor ] > nBrwWidth
               exit
            endif
            oCol := ::ColAtPos( nFor )
            if !lFinDatos //.or. ::lRowDividerComplete
               oCol:PaintData( nRow, aCols[ nFor ], nHeight, lHighLite, .f., nFor, nRowPos )
            endif
         next

         nRowPos++
         nRow += nRowHeight

         if ::Skip() == 0
            lFinDatos := .T.
            if !::lRowDividerComplete
               exit
            endif
         endif

      enddo

      if nMarqStyle <= MARQSTYLE_HIGHLCELL .and. aCols[ nLast + 1 ] < nBrwWidth .and. ! ::lTransparent
         hBrush := CreateColorBrush( ::nClrPane )
         nTemp  := aCols[nLast + 1] - 1
         FillRect( hDC, { nFirstRow, nTemp, ::BrwHeight() - ::FooterHeight(), nBrwWidth }, hBrush )
         DeleteObject( hBrush )
      else
         // �?
      endif

      if hSelBrush != nil
         DeleteObject( hSelBrush )
      endif

      ::nDataRows := nRowPos - 1
      ::nRowSel := Max( Min( ::nRowSel, ::nDataRows ), 1)

      if nRow < nLastRow
         ::EraseData( nRow  )
      endif

      Eval( ::bBookMark, nBookMark )

      ::DrawLine( .t. )

      /*
      Paint lines
      */

      do case
      case nColStyle == LINESTYLE_NOLINES
         nTemp := 2
      case nColStyle < LINESTYLE_INSET
         nTemp := 1
      otherwise
         nTemp := 0
      end case


      if nColStyle > 0
         if ::lColDividerComplete
            nHeight := nLastRow
         else
            nHeight := ( ::nRowHeight * ( nRowPos - 1 ) ) + nFirstRow
         endif
         for nFor := 2 to nLast + 1
            nCol := aCols[ nFor ]
            if nColStyle != LINESTYLE_RAISED
               DrawVert( hDC, nCol - 2, nFirstRow, nHeight, hColPen )
            else
               DrawVert( hDC, nCol - 2, nFirstRow, nHeight, hWhitePen )
               DrawVert( hDC, nCol - 1, nFirstRow, nHeight, hColPen )
            endif
            if nColStyle = LINESTYLE_INSET
               DrawVert( hDC, nCol - 1, nFirstRow, nHeight, hWhitePen )
            endif
         next
      endif

      if nRowStyle > 0
         nRow   := ::HeaderHeight() - 1
         nTemp2 := ::nDataRows
         do while nTemp2-- > 0
            nRow += nRowHeight
            if lRecSel
               DrawHorz( hDC, nRow,     2, ::nRecSelWidth - 4, hGrayPen  )
               DrawHorz( hDC, nRow + 1, 2, ::nRecSelWidth - 4, hWhitePen )
            endif
            for nFor := 1 to nLast
               if ::aCols[ nFor ]:HasBorder( ::nDataRows - nTemp2 )
                  nCol   := aCols[ nFor ] - If( nFor != 1, nTemp, 0 )
                  nWidth := nCol + ::ColAtPos( nFor ):nWidth + If( nFor != 1, nTemp, 0 )
                  if nRowStyle != LINESTYLE_RAISED
                     DrawHorz( hDC, nRow, nCol, nWidth, hRowPen )
                  else
                     DrawHorz( hDC, nRow,     nCol, nWidth, hWhitePen )
                     DrawHorz( hDC, nRow - 1, nCol, nWidth, hRowPen   )
                  endif
                  if nRowStyle = LINESTYLE_INSET
                     DrawHorz( hDC, nRow - 1, nCol, nWidth, hWhitePen )
                  endif
               endif
            next
            if nMarqStyle >= MARQSTYLE_HIGHLROWRC .and. nLast == Len( ::aDisplay )
               nCol   := aCols[ nFor ] - nTemp
               nWidth := ::BrwWidth() - 4
               if nRowStyle != LINESTYLE_RAISED
                  DrawHorz( hDC, nRow, nCol, nWidth, hRowPen )
               else
                  DrawHorz( hDC, nRow,     nCol, nWidth, hWhitePen )
                  DrawHorz( hDC, nRow - 1, nCol, nWidth, hRowPen   )
               endif
               if nRowStyle = LINESTYLE_INSET
                  DrawHorz( hDC, nRow - 1, nCol, nWidth, hWhitePen )
               endif
            endif
         enddo
      endif

   ::DispEnd( aInfo )

return 0

//----------------------------------------------------------------------------//

METHOD DrawLine( lSelected, nRowSel ) CLASS TXCBrowse

   local oCol
   local nRow, nCol, nFor, nLast, nHeight, nStyle, nWidth,;
         nColStyle, nTemp, nDataHeight, nRight
   local hDC, hBrush, hWhitePen, hColPen
   local lHighLite

   DEFAULT lSelected := .f.,;
           nRowSel   := ::nRowSel

   if ::nLen == 0 .and. !::lRowDividerComplete
      return nil
   endif

   if ! ::lDrawSelected
      return  nil
   endif

   //nRefresh++

   nHeight     := ::nRowHeight
   nDataHeight := ::DataHeight
   nRow        := ( ( nRowSel - 1 ) * nHeight ) + ::HeaderHeight()

   if nRow > ::LastRow()
      return nil
   endif

   hDC       := ::GetDC()
   nLast     := ::LastDisplayPos()
   nStyle    := ::nMarqueeStyle

   //lHighLite := ( nStyle >= MARQSTYLE_HIGHLROWRC .and. lSelected ) // Why this line ?????

   //lHighLite := ( nStyle >= MARQSTYLE_HIGHLROWRC .and. lSelected )    //Cambiada m�s abajo por Cnl: 16/02/2005


   if ::lMultiSelect .and. ( nStyle == MARQSTYLE_HIGHLROWMS .or. ::nMarqueeStyle == MARQSTYLE_HIGHLWIN7 )

      lHighLite := ( Ascan( ::aSelected, Eval( ::bBookMark ) ) > 0 )
   endif

   lHighLite := ( nStyle >= MARQSTYLE_HIGHLROWRC .and. lSelected )   // Cambiada aqui por Cnl: no cogia el color
                                                                     // del Foco con Btt-dcho ni en el inicio

/*
   if nStyle == MARQSTYLE_HIGHLROWMS .or. ::nMarqueeStyle == MARQSTYLE_HIGHLWIN7
      lHighLite := ( Ascan( ::aSelected, Eval( ::bBookMark ) ) > 0 )
   endif
*/

   for nFor := 1 to nLast
      oCol := ::ColAtPos ( nFor )
      oCol:PaintData( nRow, nil, nDataHeight, lHighLite, lSelected, nFor, nRowSel )
   next

   if nStyle >= MARQSTYLE_HIGHLROWRC .and. nStyle != MARQSTYLE_HIGHLWIN7
      nColStyle := ::nColDividerStyle
      nCol      := oCol:nDisplayCol + oCol:nWidth + 2
      nWidth    := ::BrwWidth() - 2
      if nColStyle < LINESTYLE_INSET
         nCol--
         nWidth++
      endif
      nTemp := nRow + nDataHeight
      if nCol < nWidth
         if lHighLite
            if ::hWnd == GetFocus()
               hBrush := CreateColorBrush( Eval( If( ::bClrRowFocus == nil, ::bClrSelFocus, ::bClrRowFocus ) )[ 2 ] )
            else
               hBrush := CreateColorBrush( Eval( ::bClrSel )[ 2 ] )
            endif
         else
            hBrush := CreateColorBrush( Eval( ::bClrStd )[ 2 ] )
         endif
         if lHighLite .or. ! ::lTransparent
            FillRect( hDC, {nRow, nCol, nTemp, nWidth }, hBrush )
         endif
         DeleteObject( hBrush )
      endif
      if nStyle == MARQSTYLE_HIGHLROWMS
         nCol := iif(::lRecordSelector, ::nRecSelWidth - 1, 0 )
         if lSelected
            FrameDot(hDC, nRow, nCol, nRow + nDataHeight - 1, nWidth - 1)
         elseif nColStyle > 0 // We have to FullPaint the line cols :-((
            hColPen   := ::hColPen
            hWhitePen := ::hWhitePen
            for nFor := 1 to nLast
               oCol := ::ColAtPos ( nFor )
               nCol := oCol:nDisplayCol + oCol:nWidth
               if nColStyle != LINESTYLE_RAISED
                  DrawVert( hDC, nCol, nRow, nRow + nDataHeight, hColPen )
               else
                  DrawVert( hDC, nCol,     nRow, nRow + nDataHeight, hWhitePen )
                  DrawVert( hDC, nCol + 1, nRow, nRow + nDataHeight, hColPen   )
               endif
               if nColStyle = LINESTYLE_INSET
                  DrawVert( hDC, nCol + 1, nRow, nRow + nDataHeight, hWhitePen )
               endif
            next
         endif
      endif
   endif

   if ::lRecordSelector
      if lSelected
         /*
         PalBmpDraw( hDC, nRow + ( nHeight / 2 ) - 8, ::nWRecordSel - ::nSpRecordSel,;
                     ::hBmpRecSel, 0, 9, 14,, .t., ::nRecSelColor )
         */

         PalBmpDraw( hDC, nRow + ( nHeight / 2 ) - 8, ::nRecSelWidth - 15 - ::nSpRecordSel,;
                     ::hBmpRecSel, 0, 16, 16,, .t., ::nRecSelColor )

      else
         /*
          FillRect( hDC,;
                   {nRow + 1, ::nWRecordSel - ::nSpRecordSel,;
                    nRow + nDataHeight - 1 , ::nWRecordSel - 3},;
                    ::hBrushRecSel )
         */

         FillRect( hDC, { nRow + 1,;
                          ::nRecSelWidth - 15 - ::nSpRecordSel, nRow + nDataHeight - 1, ::nRecSelWidth - 3 },;
                          ::hBrushRecSel )

      endif

   endif

   if lSelected

      nHeight -= 2
      oCol := ::ColAtPos( ::nColSel )
      do case
         case nStyle == MARQSTYLE_DOTEDCELL
              oCol:Box( nRow, nil, nDataHeight, 1 )
         case nStyle == MARQSTYLE_SOLIDCELL
              oCol:Box( nRow, nil, nDataHeight, 2 )
         case nStyle == MARQSTYLE_HIGHLCELL
              oCol:PaintData( nRow, nil, nDataHeight, .t., .t. , ::nColSel, nRowSel )
         case nStyle == MARQSTYLE_HIGHLROWRC
              oCol:Box( nRow, nil, nDataHeight, 3 )
         case nStyle == MARQSTYLE_HIGHLWIN7
              oCol     := ::ColAtPos( nLast )
              nLast    := Min( oCol:nDisplayCol + oCol:nWidth, ::BrwWidth() )
/*
         RoundBox( hDC, 2, nRow - 1, ::BrwWidth() - 1, nRow + nDataHeight,     2, 2,;
                   RGB( 235, 244, 253 ), 1 )
         RoundBox( hDC, 1, nRow - 2, ::BrwWidth(),     nRow + nDataHeight + 1, 2, 2,;
                   RGB( 125, 162, 206 ), 1 )
*/
         RoundBox( hDC, 2, nRow - 1, nLast - 1, nRow + nDataHeight,     2, 2,;
                        RGB( 229,243,251), 1 )// 209,232,255 ), 1 ) //235, 244, 253 ), 1 )
         RoundBox( hDC, 1, nRow - 2, nLast,     nRow + nDataHeight + 1, 2, 2,;
                        RGB( 125, 162, 206 ), 1 )

      endcase
   /*
   else

      if nStyle == MARQSTYLE_HIGHLCELL
         oCol:Box( nRow, nil, nDataHeight, 0 )
      endif
      */
   endif

   ::ReleaseDC()

return nil

//----------------------------------------------------------------------------//

METHOD PaintHeader( hDC, aCols, nLast, hWhitePen, hGrayPen, hColPen ) CLASS TxCBrowse

   local nRow, nCol, oCol, nHeight, nBrwWidth, aGroup
   local hHeaderPen, aColors, hBrush
   local nFor, nAt
   local cGrpHdr, nGrpHt, nGrpFrom := 0

   nBrwWidth   := ::BrwWidth()
   aColors     := Eval( ::bClrHeader )
   hBrush      := CreateColorBrush( aColors[ 2 ] )
   hHeaderPen  := CreatePen( PS_SOLID, 1, aColors[ Min( 3, Len( aColors ) ) ] )
   nRow    := 0
   nHeight := ::nHeaderHeight - 3 // Caution: Do not change -3 in a haste. This adjusts 3 pixels added in Adjust method
   if !Empty( ::nRowDividerStyle ) .and. ::lLinHeadHSep
      DrawHorz( hDC, nRow, 2, nBrwWidth, hGrayPen )
   endif
   nRow++
   if !Empty( ::nRowDividerStyle ) .and. ::lLinHeadHSep
      DrawHorz( hDC, nRow, 2, nBrwWidth, hWhitePen )
   endif
   nRow++
   if ::lLinHeadHSep
   FillRect( hDC, { nRow, 2, nRow + nHeight, nBrwWidth}, hBrush ) //::hBrushRecSel )
   else
   FillRect( hDC, { nRow-2, 2, nRow + nHeight+1, nBrwWidth}, hBrush ) //::hBrushRecSel )
   endif

   for nFor := 1 to nLast
      nCol     := aCols[ nFor ]
      oCol     := ::ColAtPos( nFor )
      if ::lLinHeadVSep     // A�adida por Cnl: 16/02/2005
         DrawVert( hDC, nCol - 2, nRow + 1, nRow + nHeight - 2, hGrayPen  )
         DrawVert( hDC, nCol - 1, nRow + 1, nRow + nHeight - 2, hWhitePen )
      endif
      oCol:PaintHeader( nRow, nCol, nHeight, .f., hDC )

      if oCol:cGrpHdr != cGrpHdr
         cGrpHdr     := oCol:cGrpHdr
         if Empty( oCol:cGrpHdr )
            nGrpFrom       := 0
            aGroup         := nil
         else
            nGrpFrom       := nCol
            nGrpHt         := oCol:nGrpHeight
            nAt            := AScan( ::aHeaderTop, { |a| a[ 3 ] == oCol:nPos } )
            aGroup         := If( nAt > 0, ::aHeaderTop[ nAt ], nil )
         endif
      endif

      if nFor == nLast .or. oCol:cGrpHdr != ::ColAtPos( nFor + 1 ):cGrpHdr
         if ! Empty( cGrpHdr )
            // paint group header
            oCol:PaintHeader( nRow, nGrpFrom, nHeight, .f., hDC, aCols[ nFor + 1 ] - nGrpFrom, ;
                              If( aGroup == nil, nil, aGroup[ 6 ] ) )
            if ::lLinHeadHSep     // A�adida por Cnl: 16/02/2005
               DrawHorz( hDC, nRow + nGrpHt, nGrpFrom - 2, aCols[ nFor + 1 ] - 2, hHeaderPen )
            endif
         endif
      endif
   next nFor

   nCol     := aCols[ nFor ]
   if ::lLinHeadVSep     // A�adida por Cnl: 16/02/2005
      DrawVert( hDC, nCol - 2,       nRow + 1, nRow + nHeight - 2, hGrayPen  )
      DrawVert( hDC, nCol - 1,       nRow + 1, nRow + nHeight - 2, hWhitePen )
   endif
   if !Empty( ::nRowDividerStyle )
      if ::lLinHeadHSep     // A�adida por Cnl: 16/02/2005
         DrawHorz( hDC, nRow + nHeight, 0,        nBrwWidth - 2,      hGrayPen  )
      endif
   endif

   DeleteObject( hBrush )
   DeleteObject( hHeaderPen )

return nil

//------------------------------------------------------------------//

METHOD PaintFooter( hDC, aCols, nLast, nBrwWidth, nBrwHeight, hWhitePen, hGrayPen ) CLASS TXCBrowse

   local nRow, nCol, nFor, oCol
   local nHeight, hBrush, aColors

   nHeight  := ::nFooterHeight - 3 // Caution: Do not change -3 in a haste. This adjusts 3 pixels added in Adjust method
   nRow     := nBrwHeight - ::nFooterHeight
   aColors := Eval( ::bClrFooter )
   if !Empty( ::nRowDividerStyle )
      DrawHorz( hDC, nRow, 0, nBrwWidth, hGrayPen )
   endif
   nRow++
   if !Empty( ::nRowDividerStyle ) .and. ::lLinFootHSep
      DrawHorz( hDC, nRow, 0, nBrwWidth, hWhitePen )
   endif
   nRow++
   if !Empty( ::nRowDividerStyle ) .and. ::lLinFootHSep
      DrawHorz( hDC, nRow + nHeight, 0, nBrwWidth, hGrayPen )
   endif
   hBrush  := CreateColorBrush( aColors[ 2 ] )
   FillRect( hDC, { nRow, 1, nRow + nHeight, nBrwWidth}, ::hBrushRecSel ) // col 0 is painted with vert line. So paint has to start at col 1 ( not col 0 )
   DeleteObject( hBrush )
   for nFor := 1 to nLast
      nCol := aCols[ nFor ]
      oCol := ::ColAtPos( nFor )
      if ::lLinFootVSep
         DrawVert( hDC, nCol - 2, nRow + 1, nRow + nHeight - 2, hGrayPen )
         DrawVert( hDC, nCol - 1, nRow + 1, nRow + nHeight - 2, hWhitePen )
      endif
      oCol:PaintFooter( nRow, nCol, nHeight )
   next
   nCol := aCols[ nFor ]
   if !Empty( ::nColDividerStyle ) .and. ::lLinFootVSep
      DrawVert( hDC, nCol - 2, nRow + 1, nRow + nHeight - 2, hGrayPen )
      DrawVert( hDC, nCol - 1, nRow + 1, nRow + nHeight - 2, hWhitePen )
   endif

return nil

//------------------------------------------------------------------//

METHOD GetDisplayCols() CLASS TXCBrowse
local aDisplay
local nCols
      aDisplay := ::Super():GetDisplayCols()
      nCols    := Len( ::aCols )
      if nCols = 1
         if ::oHScroll != nil
            if ::aCols[1]:nWidth >= ::nWidth
               ::oHScroll:SetRange( 1, ::aCols[1]:nWidth )
               ::lOneCol    := .T.
            endif
         endif
      else
         ::lOneCol  := .F.       // Para implementar el scroll horizontal en una
                                 // sola columna
      endif

return aDisplay

//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//

METHOD MouseWheel( nKey, nDelta, nXPos, nYPos ) CLASS TXCBrowse

   local nMove    := Int( nDelta / 3 )
   local n, oBtn
   if ::lWheelH
      //Se puede hacer con Scroll de la ventana o dejar que el control lo haga:
      //For n = 1 to nMove
          ::KeyDown( if( nDelta < 0, VK_RIGHT, VK_LEFT ) ,  )
      //Next n
   else
      ::Super:MouseWheel( nKey, nDelta, nXPos, nYPos )
   endif
return nil

//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//
/*
METHOD LButtonDown( nRow, nCol, nFlags ) CLASS TXCBrowse


return 0
*/
//----------------------------------------------------------------------------//
/*
METHOD RButtonDown( nRow, nCol, nKeyFlags ) CLASS TXCBrowse


return 0
*/
//----------------------------------------------------------------------------//

METHOD KeyChar( nKey, nFlags ) CLASS TXCBrowse
Local uVal   := 0
Local uRet

   do case

      case nKey == VK_RETURN
           if !empty( ::bKeyChar )
              uRet  := Eval( ::bKeyChar,  ,  ,  ,  )
           else
              if ::bLDblClick != nil
                 Eval( ::bLDblClick )
              else
                 ::Super:KeyChar( nKey, nFlags )
              endif
           endif
      case nKey == VK_TAB

      Otherwise

          ::Super:KeyChar( nKey, nFlags )

   endcase

Return uVal

//------------------------------------------------------------------------------//

METHOD GetDlgCode( nLastKey ) CLASS TXCBrowse

   // This method is very similar to TControl:GetDlgCode() but it is
   // necessary to have WHEN working

   if .not. ::oWnd:lValidating
      if nLastKey == VK_UP .or. nLastKey == VK_DOWN ;
         .or. nLastKey == VK_RETURN .or. nLastKey == VK_TAB
         ::oWnd:nLastKey = nLastKey
      else
         ::oWnd:nLastKey = 0
      endif
   endif

return If( IsWindowEnabled( ::hWnd ), If( ::nDlgCode != nil, ::nDlgCode, DLGC_WANTALLKEYS ), 0 )
       //If( IsWindowEnabled( ::hWnd ), DLGC_WANTALLKEYS, 0 )


/*
And in your app, simply do:
oBrw:nDlgCode = 1 // DLGC_WANTARROWS

You could also try with:
oBrw:nDlgCode = 0x0080 // DLGC_WANTCHARS
*/

//----------------------------------------------------------------------------//


METHOD KeyDown( nKey, nFlags ) CLASS TXCBrowse
Local nColNext
Local nRet


   if !Empty( ::bKeyDown ) .and. ValType( ::bKeyDown ) = "B"

      nRet := Eval( ::bKeyDown , nKey, nFlags )
      // En funcion del valor retornado, seguir ejecutando este metodo
      // o hacer un ::Super:bKeyDown, o solo un return 0
   endif

   //? nKey, nFlags
   ::Super:KeyDown( nKey, nFlags )

   do case

      case nKey == VK_TAB

      case nKey == VK_UP
            //::Super:KeyDown( nKey, nFlags )
            if ::lNoEmpty
              /*
              if ::nRowSel > 1
                 if ValType( ::aCols[::nColSel ]:bBmpData ) = "U"
                    ::KeyDown( nKey, nFlags )
                 else
                    if Len( ::aCols[::nColSel ]:aBitmaps ) < ::nRowSel
                       ::KeyDown( nKey, nFlags )
                    endif
                 endif
              else
                 // Ir a la ultima fila ?
              endif
              */
           else

           endif
      case nKey == VK_DOWN
           //::Super:KeyDown( nKey, nFlags )
           if ::lNoEmpty

              if ::nRowSel < Len( ::aCols[::nColSel ]:aBitmaps ) .or. ;
                 Len( ::aCols[::nColSel ]:aBitmaps ) > 0
                 if ValType( ::aCols[::nColSel ]:bBmpData ) = "U" .and. ;
                    ValType( ::aCols[::nColSel ]:bStrData ) = "U" .and. ;
                    ValType( ::aCols[::nColSel ]:bStrImage) = "U"

                    //::KeyDown( VK_UP, nFlags )

                 else
                    //if ::nColSel > 1
                       //::GoLeft()
                    //else
                       //::GoRight()
                    //endif
                    //::Super:KeyDown( nKey, nFlags )
                 endif
              else
                 // Ir a la ultima fila ?
              endif

           else
              //::Super:KeyDown( nKey, nFlags )
           endif
      case nKey == VK_LEFT
           //::Super:KeyDown( nKey, nFlags )
            //bStrData, bBmpData and bStrImage
           if ::lNoEmpty
              if ::nColSel > 1
                 if ValType( ::aCols[::nColSel ]:bBmpData ) = "U" .and. ;
                    ValType( ::aCols[::nColSel ]:bStrData ) = "U" .and. ;
                    ValType( ::aCols[::nColSel ]:bStrImage) = "U"
                    ::KeyDown( nKey, nFlags )
                 else
                    if Len( ::aCols[::nColSel ]:aBitmaps ) < ::nRowSel
                       //::KeyDown( nKey, nFlags )
                    endif
                 endif
              else
                 // Ir a la ultima columna ?
              endif
           endif
      case nKey == VK_RIGHT
           //::Super:KeyDown( nKey, nFlags )
           if ::lNoEmpty
              if ::nColSel < Len( ::aCols )
                 if ValType( ::aCols[::nColSel ]:bBmpData ) = "U" .and. ;
                    ValType( ::aCols[::nColSel ]:bStrData ) = "U" .and. ;
                    ValType( ::aCols[::nColSel ]:bStrImage) = "U"
                    ::KeyDown( nKey, nFlags )
                 else
                    if Len( ::aCols[::nColSel ]:aBitmaps ) < ::nRowSel
                       //::KeyDown( nKey, nFlags )
                    endif
                 endif
              else
                 // Ir a la primera columna ?
              endif
           endif
      case nKey == 17             // Tecla ALT GR   y tambien CTRL LEFT
           if nFlags = 1900545   //
              ? "ALT GR / CTRL L"
           endif

      case nKey == 17             // CTRL RIGHT
           if nFlags = 18677761  //
              ? "CTRL R"
           endif

      case nKey == 91           // Tecla L WINDOW
           if nFlags = 22740993 //
              ? "WINDOW"
           endif

      case nKey == 93           // Tecla VK_MENU
           if nFlags = 22872065 //VK_MENU
              ? "R MENU"
              //ShowWindow( GetDesktopWindow(), 1 ) //SW_NORMAL )
           endif
      //case nKey ==

      Otherwise
          //? nKey, nFlags
          ::Super:KeyDown( nKey, nFlags )

   endcase

Return 0

//----------------------------------------------------------------------------//
/*
METHOD GoUp( nUp ) CLASS TXCBrowse


return nUp  //nil
*/
//----------------------------------------------------------------------------//
/*
METHOD GoDown( nDown ) CLASS TXCBrowse


return nDown  //nil
*/
//----------------------------------------------------------------------------//

METHOD MouseMove( nRow, nCol, nKeyFlags ) CLASS TXCBrowse
   //local nKey      := nKeyFlags
   local nMousePos := ::MouseRowPos( nRow )

   if ::lDrag .or. ::lEditMode
      return ::Super:MouseMove( nRow, nCol, nKeyFlags )
   endif

   //if !Empty( ::OnMouseMove )  .and. ValType( ::OnMouseMove ) = "B"   //::bMMoved )
   if !Empty( ::bMMoved )  .and. ValType( ::bMMoved ) = "B"   //::bMMoved )
      Eval( ::bMMoved, nRow, nCol, nKeyFlags, Self )
      //Eval( ::OnMouseMove, Self, nRow, nCol, nKeyFlags )
   endif
   ::Super:MouseMove( nRow, nCol, nKeyFlags )

return 0

//----------------------------------------------------------------------------//
/*
METHOD HandleEvent( nMsg, nWParam, nLParam ) CLASS TXCBrowse

   //if nMsg == WM_MOUSELEAVE
   //   return ::MouseLeave( nHiWord( nLParam ), nLoWord( nLParam ), nWParam )
   //endif
   if nMsg == WM_MOUSEMOVE

   endif
   if nMsg == WM_NCMOUSEMOVE

   endif

return ::Super:HandleEvent( nMsg, nWParam, nLParam )
*/
//----------------------------------------------------------------------------//

METHOD ColNoEmpty( nDir ) CLASS TXCBrowse    // nDir -> 1 Right -- -1 Left
Local nCol   := 0
Local x
Local y
Local z
DEFAULT nDir := 1
z     := nDir
y     := IF( nDir > 0, Len( ::aCols ) , 1 )
For x = ::nColSel to y step z       // Valorar contenido celda no de columna
    if ValType( ::aCols[ x ]:bBmpData ) <> "U"
       nCol := x
    endif
Next x

//if Empty( ::aCols[ n ]:Value() )

/*
if Empty( nCol )
   z    := -nDir
   y    := IF( nDir > 0, Len( ::aCols ) , 1 )
   For x = ::nColSel to y step z
       if ValType( ::aCols[ x ]:bBmpData ) <> "U" // if Len( ::aCols[::nColSel ]:aBitmaps ) < ::nRowSel
          nCol := x
       endif
   Next x
endif
*/
Return nCol

//----------------------------------------------------------------------------//

METHOD RowNoEmpty( nDir ) CLASS TXCBrowse    // nDir -> 1 Down -- -1 Up
Local nRow
Local x
Local y
Local z
DEFAULT nDir := 1
z     := nDir
y     := IF( nDir > 0, Len( ::nLen ) , 1 )

For x = ::nRowSel to y step z       // Valorar contenido celda no de columna
    if ValType( ::aCols[ x ]:bBmpData ) <> "U"
       nRow  :=  x
    endif
Next x

//if Empty( ::aCols[ n ]:Value() )

/*
if Empty( nRow )
   z    := -nDir
   y    := IF( nDir > 0, Len( ::nLen ) , 1 )
   For x = ::nRowSel to y step z
       if ValType( ::aCols[ x ]:bBmpData ) <> "U"
          nCol := x
       endif
   Next x
endif
*/
Return nRow

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//
//----------------------------------------------------------------------------//

METHOD SetColumns( nRow, nCol, nFlags ) CLASS TXCBrowse

   local oMenu, oCol
   local nFor, nLen
   local cText      := ""
   local oItem

   nLen := Len( ::aCols )

   MENU oMenu POPUP
   oMenu:l2007    := ::l2007
   oMenu:l2010    := !::l2007
      for nFor := 1 to nLen
         oCol := ::aCols[ nFor ]
         if !Empty( oCol:cHeader ) //.and. Empty( oCol:cGrpHdr )
            MenuAddItem( oCol:cHeader, , !oCol:lHide, ;
               ( Len(::aDisplay) != 1 .or. oCol:nPos != 1 ), ;
               GenMenuBlock( ::aCols, nFor, Self ) )
         else
            if !Empty( oCol:cGrpHdr )
               if Empty( cText )
                  cText  := oCol:cGrpHdr
                  MenuAddItem( cText, , !oCol:lHide, ;
                     ( Len(::aDisplay) != 1 .or. oCol:nPos != 1 ), ;
                     GenMenuBlock( oCol, nFor, Self ) )
               endif
            else
               cText  := ""
               /*
               oItem := MenuAddItem( " - ", , !oCol:lHide, ;
                  ( Len(::aDisplay) != 1 .or. oCol:nPos != 1 ), ;
                  GenMenuBlock( ::aCols, nFor, Self ) )
               oItem:bWhen := { || .F. }
               */
            endif
         endif
      next
   ENDMENU

   ACTIVATE POPUP oMenu AT nRow, nCol OF Self

return nil

//----------------------------------------------------------------------------//

METHOD AddColumn( cHead, bData, cPic, uClrFore, uClrBack, ;
                  cAlign, nWidth, lBitmap, lEdit, bOnPostEdit,  ;
                  cMsg, bWhen, bValid, cErr, lHilite, ncOrder,;
                  nAt, bBmpData, aBmp, lHide, nTot, bFooter, uCargo ) CLASS TXCBrowse

   local oCol, uTemp

   // This method is intended only to support command syntax
   // and this method should not be called directly in the
   // application program

   if ValType( nAt ) == 'O' .and. nAt:IsKindOf( TXCBrwColumn() ) // Upper( nAt:className ) == "TXBRWCOLUMN"
      nAt   := nAt:nCreationOrder
   endif
   if ValType( nAt ) == 'N' .and. nAt > 0 .and. nAt <= Len( ::aCols )
      oCol  := ::InsCol( nAt )
   else
      oCol  := ::AddCol()
   endif

   oCol:cHeader      := IfNil( cHead, '' )
   oCol:cEditPicture := cPic

   if bData != nil
      if lBitmap .and. bBmpData == nil
         oCol:bBmpData        := bData
      else
         if ValType( bData ) == 'N'
            oCol:nArrayCol    := bData
         else
            oCol:bEditValue   := bData
         endif
      endif
   endif

   if bBmpData != nil
      oCol:bBmpData  := bBmpData
   endif

   if cAlign != nil
      cAlign            := Upper( Left( cAlign, 1 ) )
      cAlign            := iif( cAlign == 'R', AL_RIGHT, iif( cAlign == 'C', AL_CENTER, nil ) )
      if cAlign != nil
         oCol:nHeadStrAlign := oCol:nDataStrAlign := oCol:nFootStrAlign := cAlign
      endif
   endif

   if ValType( nWidth ) == 'N'
      oCol:nWidth       := nWidth
   endif

   if lEdit .or. ValType( bOnPostEdit ) == 'B' .or. ValType( bValid ) == 'B' .or. ValType( bWhen ) == "B"
      oCol:nEditType   := 1
      oCol:bEditWhen   := bWhen
      oCol:bEditValid  := bValid
      if bOnPostEdit == nil
         oCol:bOnPostEdit  := { |o,x,n| If( n != VK_ESCAPE .and. Eval( o:oBrw:bLock ), ;
          o:Value := x, ) }

      else
         oCol:bOnPostEdit := bOnPostEdit
      endif

   endif

   if valtype( uClrFore ) == 'N'
      if valtype( uClrBack ) == 'N'
         oCol:bClrStd   := {|| {uClrFore,uClrBack} }
      elseif valtype( uClrBack ) == 'B'
         oCol:bClrStd   := { || {uClrFore,eval(uClrBack)} }
      else
         oCol:bClrStd   := { || { uClrFore, eval( oCol:oBrw:bClrStd )[2] } }
      endif
   elseif valtype( uClrFore ) == 'B'
      if uClrBack == nil
         if valtype( uTemp := eval( uClrFore ) ) == 'A' .and. Len( uTemp ) == 2
            oCol:bClrStd   := uClrFore
         else
            oCol:bClrStd   := { || { eval(uClrFore), eval( oCol:oBrw:bClrStd )[2] } }
         endif
      elseif valtype( uClrBack ) == 'N'
         oCol:bClrStd      := { || { eval(uClrFore), uClrBack } }
      elseif valtype( uClrBack ) == 'B'
         oCol:bClrStd      := { || { eval(uClrFore), eval(uClrBack) } }
      endif
   endif

   if ncOrder != nil
      oCol:cSortOrder := ncOrder
      if ::nDataType == DATATYPE_RDD .and. ! Empty( ::cAlias ) .and. SELECT( ::cAlias ) > 0
         oCol:cOrdBag    := ( ::cAlias )->( OrdBagName( ncOrder ) )
      endif
   endif

   if ValType( aBmp ) == 'A'
      oCol:AddBitmap( aBmp )
   endif

   oCol:lHide     := lHide

   if ValType( nTot ) == 'N'
      oCol:nTotal := nTot
      oCol:lTotal  := .t.
   endif
   if ValType( bFooter ) == 'B'
      oCol:bFooter := bFooter
   elseif ValType( nTot ) == 'B'
      oCol:bFooter := nTot
   endif

   oCol:Cargo := uCargo

   if ::lCreated
      oCol:Adjust()
   endif

return oCol

//----------------------------------------------------------------------------//

METHOD SetTree( oTree, aResource, bOnSkip, aCols ) CLASS TXCBrowse

   local oCol, aBmp := { 0, 0, 0 }
   local n, nLevels, aBlocks, bBookMark

   DEFAULT oTree     := 2

   if ValType( oTree ) == 'N'
      if ! Empty( ::aCols ) .and. Len( ::aCols ) >= 2
         nLevels  := Min( oTree, Len( ::aCols ) )
      else
         return nil
      end
      // MakeTree
      aBlocks := Array( nLevels )
      for n := 1 to nLevels
         aBlocks[ n ] := If( ::aCols[ n ]:bStrData == nil, ::aCols[ n ]:bEditvalue, ::aCols[ n ]:bStrData )
         ::aCols[ n ]:lHide   := .t.      //Hide()
      next
      ::nLen      := Eval( ::bKeyCount )
      bBookMark   := ::bBookMark
      oTree       := SummaryDataAsTree( ::bSkip, { |nRow| nRow > ::nLen }, aBlocks, ::bBookMark )
      bOnSkip     := { || Eval( bBookMark, ::oTreeItem:cargo ) }
      ::nDataType := nOr( ::nDataType, DATATYPE_TREE )
   else
      ::nDataType := DATATYPE_TREE
   endif

   DEFAULT bOnSkip   := { || nil }

   ::oTree     := oTree
   if oTree:ClassName() = "TTREEVIEW"
      ::oTreeItem := oTree:GoTop()
      ::bGoTop    := { || ::oTreeItem := ::oTree:GoTop(), Eval( bOnSkip, ::oTreeITem ) }
      ::bGoBottom := { || ::oTreeItem := ::oTree:Select( ::oTree:aItems[ Len(::oTree:aItems) ] ), Eval( bOnSkip, ::oTreeITem ) }
      ::bBof      := { || .f. }                //GetLast()
      ::bEof      := { || .f. }
      ::bKeyCount := { || Len( ::oTree:aItems ) } //::oTree:ncount() }
   
      ::bKeyNo    := { |n| If( n == nil, BuscaItem( ::oTree:GetSelected(), ::oTree ) , ; //::oTreeItem:ItemNo() , ;
                          ( n--, ::oTreeItem := ::oTree:aItems[ BuscaItem( ::oTree:GetSelected(), ::oTree ) + 1 ], Eval( bOnSkip, ::oTreeITem ), n + 1 ) ) }
                       //  ( n--, ::oTreeItem := ::oTree:GoTop:Skip( @n ), Eval( bOnSkip, ::oTreeITem ), n + 1 ) ) }
   
      ::bBookMark := ::bKeyNo
   
      ::bSkip     := { |n| If( n == nil, n := 1, ), ;
                           ::oTreeItem := ::oTree:aItems[ BuscaItem( ::oTree:GetSelected(), ::oTree ) + n ], ;   //::oTreeItem:Skip( @n ),  ;
                           Eval( bOnSkip, ::oTreeITem ), n }

   else
      ::oTreeItem := oTree:oFirst
      ::bGoTop    := { || ::oTreeItem := ::oTree:oFirst, Eval( bOnSkip, ::oTreeITem ) }
      ::bGoBottom := { || ::oTreeItem := ::oTree:GetLast(), Eval( bOnSkip, ::oTreeITem ) }
      ::bBof      := { || .f. }
      ::bEof      := { || .f. }
      ::bKeyCount := { || ::oTree:ncount() }

      ::bKeyNo    := { |n| If( n == nil, ::oTreeItem:ItemNo() , ;
                          ( n--, ::oTreeItem := ::oTree:oFirst:Skip( @n ), Eval( bOnSkip, ::oTreeITem ), n + 1 ) ) }

      ::bBookMark := ::bKeyNo
      ::bSkip     := { |n| If( n == nil, n := 1, ), ;
                           ::oTreeItem := ::oTreeItem:Skip( @n ),  ;
                           Eval( bOnSkip, ::oTreeITem ), n }
   endif
   
   if Empty( ::aCols )
      oCol  := ::AddCol()
   else
      oCol  := ::InsCol( 1 )
   endif

   oCol:bEditValue   := { |x| If( x == nil, ::oTreeItem:cPrompt, ::oTreeItem:cPrompt := x ) }
   oCol:cHeader      := "Item"
   oCol:nWidth       := 200
   oCol:bLDClickData := { || If( ::oTreeItem:oTree != nil,( ::oTreeItem:Toggle(), ::Refresh() ),) }
   oCol:bIndent      := { || ::oTreeItem:ItemLevel() * 20 - 20 } //nLevel * 20 - 20 }

   if ValType( aResource ) == 'A'
      oCol:AddBitmap( aResource )
   endif
   oCol:bBmpData   := { || If( ::oTreeItem:oTree == nil, 3, If( ::oTreeItem:IsExpanded(), 1, 2 ) ) } //lOpened, 1, 2 ) ) }

   ::nFreeze         := 1

   if ValType( aCols ) == 'L' .and. aCols .and. ValType( ::oTreeItem:Cargo ) == 'A'
      for n := 1 to Len( ::oTreeItem:Cargo )
         ::SetColsForTree( n )
      next n
   elseif ValType( aCols ) == 'A'
      for n := 1 to Len( aCols )
         ::SetColsForTree( aCols[ n ] )
      next n
   endif

return Self


/*
METHOD SetTree( oTree, aResource, bOnSkip, aCols ) CLASS TXBrowse

   local oCol, aBmp := { 0, 0, 0 }
   local n, nLevels, aBlocks, bBookMark

   DEFAULT oTree     := 2

   if ValType( oTree ) == 'N'
      if ! Empty( ::aCols ) .and. Len( ::aCols ) >= 2
         nLevels  := Min( oTree, Len( ::aCols ) )
      else
         return nil
      end
      // MakeTree
      aBlocks := Array( nLevels )
      for n := 1 to nLevels
         aBlocks[ n ] := If( ::aCols[ n ]:bStrData == nil, ::aCols[ n ]:bEditvalue, ::aCols[ n ]:bStrData )
         ::aCols[ n ]:lHide   := .t.      //Hide()
      next
      ::nLen      := Eval( ::bKeyCount )
      bBookMark   := ::bBookMark
      oTree       := SummaryDataAsTree( ::bSkip, { |nRow| nRow > ::nLen }, aBlocks, ::bBookMark )
      bOnSkip     := { || Eval( bBookMark, ::oTreeItem:cargo ) }
      ::nDataType := nOr( ::nDataType, DATATYPE_TREE )
   else
      ::nDataType := DATATYPE_TREE
   endif

   DEFAULT bOnSkip   := { || nil }

   ::oTree     := oTree
   ::oTreeItem := oTree:oFirst

   ::bGoTop    := { || ::oTreeItem := ::oTree:oFirst, Eval( bOnSkip, ::oTreeITem ) }
   ::bGoBottom := { || ::oTreeItem := ::oTree:GetLast(), Eval( bOnSkip, ::oTreeITem ) }
   ::bBof      := { || .f. }
   ::bEof      := { || .f. }
   ::bKeyCount := { || ::oTree:ncount() }

   ::bKeyNo    := { |n| If( n == nil, ::oTreeItem:ItemNo() , ;
                     ( n--, ::oTreeItem := ::oTree:oFirst:Skip( @n ), Eval( bOnSkip, ::oTreeITem ), n + 1 ) ) }

   ::bBookMark := ::bKeyNo
   ::bSkip     := { |n| If( n == nil, n := 1, ), ;
                     ::oTreeItem := ::oTreeItem:Skip( @n ),  ;
                     Eval( bOnSkip, ::oTreeITem ), ;
                     n }

   if Empty( ::aCols )
      oCol  := ::AddCol()
   else
      oCol  := ::InsCol( 1 )
   endif

   oCol:bEditValue   := { |x| If( x == nil, ::oTreeItem:cPrompt, ::oTreeItem:cPrompt := x ) }
   oCol:cHeader      := "Item"
   oCol:nWidth       := 200
   oCol:bLDClickData := { || If( ::oTreeItem:oTree != nil,( ::oTreeItem:Toggle(), ::Refresh() ),) }
   oCol:bIndent      := { || ::oTreeItem:nLevel * 20 - 20 }

   if ValType( aResource ) == 'A'
      oCol:AddBitmap( aResource )
   endif
   oCol:bBmpData   := { || If( ::oTreeItem:oTree == nil, 3, If( ::oTreeItem:lOpened, 1, 2 ) ) }

   ::nFreeze         := 1

   if ValType( aCols ) == 'L' .and. aCols .and. ValType( ::oTreeItem:Cargo ) == 'A'
      for n := 1 to Len( ::oTreeItem:Cargo )
         ::SetColsForTree( n )
      next n
   elseif ValType( aCols ) == 'A'
      for n := 1 to Len( aCols )
         ::SetColsForTree( aCols[ n ] )
      next n
   endif

return Self
*/


Function BuscaItem( oItem, oTree )
Local n := 1
Local x
 For x = 1 to Len( oTree:aItems )
     if oItem == oTree:aItems[ x ]
        n := x
        x := Len( oTree:aItems ) + 1
     endif
 Next x
Return n

//----------------------------------------------------------------------------//
      //
      //  Estructura del array  ::aHeaderTop
      //        1         2     3     4            5
      // { oCol:cGrpHdr, oCol, nFor, nFor, oCol:HeaderHeight( .t. ), ;
      //   oCol:aBitmap( oCol:nGrpBmpNo ) }
      //              6
//----------------------------------------------------------------------------//
static function GenMenuBlock( aCols, nFor, oXbrw )

   local oCol
   local n        := 0
   local nGrps    := 0
   local nGrp1    := 0
   local nGrp2    := 0
   local uRet     := {|| .T. }
   local aGrp     := {}

   if ValType( aCols ) = "A"
      oCol := aCols[ nFor ]
      uRet := {|| iif( oCol:lHide, oCol:Show(), oCol:Hide() ) }
   else
      if !Empty( oXbrw:aHeaderTop )
         nGrps := Len( oXbrw:aHeaderTop )
         For n = 1 to nGrps
             if nFor >= oXbrw:aHeaderTop[ n ][ 3 ] .and.  ;
                nFor <= oXbrw:aHeaderTop[ n ][ 4 ]
                nGrp1 := oXbrw:aHeaderTop[ n ][ 3 ]
                nGrp2 := oXbrw:aHeaderTop[ n ][ 4 ]
                n := nGrps + 1
             endif
         Next n
         n := 0
         if !Empty( nGrp1 ) .and. !Empty( nGrp2 )
            For n = nGrp2 to nGrp1 step -1
                oCol := oXbrw:aCols[ n ]
                AAdd( aGrp, oCol )
            Next n

            if nGrp2 < Len( oXbrw:aCols )
               n := nGrp2 + 1
            else
               n := nGrp1 - 1
            endif
            if Empty( oXbrw:aCols[ n ]:aBitmaps )  //? oXbrw:aCols[ n ]:Value()
               AAdd( aGrp, oXbrw:aCols[ n ] )
            endif

            if !Empty( aGrp )
            uRet := {|| AEval( aGrp, {|oC| iif(oC:lHide, oC:Show(), oC:Hide()) } )}
            endif
         endif
      endif
   endif

return uRet

//----------------------------------------------------------------------------//

Static function XBrwMnuPopup( nRow, nCol, oDlg )
Local oXBrwP
DEFAULT oDlg := Nil

   oXBrwP := TXCBrowse():New( oDlg )

   WITH OBJECT oXBrwP
      :nTop       := nRow
      :nLeft      := nCol
      :nBottom    := nRow + 100
      :nRight     := nCol + 100




      :CreateFromCode()
   END

Return oXBrwP

//----------------------------------------------------------------------------//

CLASS TXCBrwColumn FROM TXBrwColumn


   DATA bLClickData     // codeblock to be evaluated when left  clicking on the data             : A�adido Cnl-> 16/02/2005
   DATA lSetBmpCol      // Poner el Bitmap de la Columna 1 en la del Selector de Registro ¿?: Añadida por Cnl ->  16/02/2005
                        // No es lo correcto: deberia ser un bitmap para esa columna y no usar el de la columna 1
   DATA lMergeHorz      //::lMergeVert
   DATA aMergeHorz
   DATA nHGrpAlign      INIT 0

   DATA lLinHT
   DATA lLinHB
   DATA lLinVL
   DATA lLinVR
   DATA aItemTxt
   DATA cCadTxt

   DATA lControlUser
   DATA cControlUser
   DATA oControlUser
   DATA bControlUser
   DATA lCtrlCreated


   METHOD New( oBrw )   // Creates a new instance of the class
   //METHOD End()         // Destroys the object
   METHOD PaintHeader( nRow, nCol, nHeight, lInvert, hDC, nGrpWidth, aBitmap )
   METHOD PaintFooter( nRow, nCol, nHeight, lInvert )
   METHOD PaintCell( nRow, nCol, nHeight, lHighLite, lSelected, nOrder, nPaintRow )

   //METHOD PaintData( nRow, nCol, nHeight, lHighLite, lSelected, nOrder, nPaintRow )

   //METHOD Edit( nKey )

   METHOD PintaLineasHV()
   METHOD CalcClrPen()

//   METHOD DrawTxtRow( oCol, hDC, cText, aCoors , cFont, nHFont, aItems, lEnBmp,;
//                  lLinHT, lLinHB, lLinVL, lLinVR )

ENDCLASS



//----------------------------------------------------------------------------//

METHOD New( oBrw ) CLASS TXCBrwColumn

   ::Super:New( oBrw )

   ::lSetBmpCol   := .F.       // A�adida por Cnl -> 16/02/2005
   ::lMergeHorz   := .F.       // A�adida por Cnl -> 16/08/2013
   ::aMergeHorz   := {}        // A�adida por Cnl -> 16/08/2013

   ::lLinHT       := .F.       // A�adida por Cnl -> 11/09/2013
   ::lLinHB       := .F.       // A�adida por Cnl -> 11/09/2013
   ::lLinVL       := .F.       // A�adida por Cnl -> 11/09/2013
   ::lLinVR       := .F.       // A�adida por Cnl -> 11/09/2013
   ::aItemTxt     := {}        // A�adida por Cnl -> 11/09/2013
   ::cCadTxt      := ""        // A�adida por Cnl -> 11/09/2013

   ::lControlUser := .F.       // A�adida por Cnl -> 11/09/2013
   ::oControlUser := Nil       // A�adida por Cnl -> 11/09/2013
   ::lCtrlCreated := .F.       // A�adida por Cnl -> 11/09/2013
   ::cControlUser := "TPanel"  // A�adida por Cnl -> 11/09/2013
   ::bControlUser := Nil       // A�adida por Cnl -> 11/09/2013


return Self

//----------------------------------------------------------------------------//

METHOD PaintHeader( nRow, nCol, nHeight, lInvert, hDC, nGrpWidth, aBitmap ) CLASS TXCBrwColumn

   local hBrush
   local oFont
   local aColors
   local cHeader
   local nWidth, nBmpRow, nBmpCol, nBmpNo, nBmpAlign
   local lOwnDC, nBottom, nStyle
   local nRowFill  := 0

   DEFAULT lInvert := .f.

   if ::bClrHeader == nil
      ::Adjust()
   endif

   if nCol != nil
      if nCol != 0
         ::nDisplayCol := nCol
      endif
   else
      nCol := ::nDisplayCol
   endif

   if ! lInvert
      aColors := Eval( ::bClrHeader )
   else
      if empty( ::bClrHeader )                             // A�adido CNL - 18/02/2013
         aColors := { If( ::oBrw:l2007, CLR_BLACK, CLR_WHITE ), CLR_BLUE }
        ::bClrHeader := {|| aColors }
        //aColors := Eval( ::bClrHeader )
      //else                                                 // A�adido CNL - 18/02/2013
         //aColors := Eval( ::bClrHeader )
      endif
      aColors := Eval( ::bClrHeader )
   endif

   if hDC == nil
      hDC := ::oBrw:GetDC()
      lOwnDC := .f.
   else
      lOwnDC := .t.
   endif

   if nGrpWidth == nil
      // normal header
      nWidth   := ::nWidth
      if ::cHeader == nil
         // This should never be the case, but some application code may assign nil to cheader
         ::cHeader := ''
      endif
      cHeader  := ::cHeader
      nRow     := nRow + ::nGrpHeight
      nHeight  := nHeight - ::nGrpHeight
      if Val( Right( FWVERSION , 5 ) ) > 14.06
         oFont    := ::HeaderFont
      else
         oFont    := ::oHeaderFont
      endif
      nStyle   := ::DefStyle( ::nHeadStrAlign, ! ( CRLF $ cHeader ) )
      nBmpNo   := If( ValType( ::nHeadBmpNo ) == 'B', Eval( ::nHeadBmpNo ), ::nHeadBmpNo )
      if ! Empty( ::cOrder )
         aBitmap     := ::oBrw:aSortBmp[ If( ::cOrder == 'A', 1, 2 ) ]
         nBmpAlign   := AL_RIGHT
      elseif !Empty( aBitmap := ::aBitmap( nBmpNo ) )
         nBmpAlign   := ::nHeadBmpAlign
      endif
   else
      // paint group header
      nWidth   := nGrpWidth
      if Empty( ::cHeader )
         // No se pone nRow para que el FillRect de abajo lo haga correctamente
         // Despues del relleno asigno nRowFill a nRow para pintar el texto
         nRowFill := nRow + ::oBrw:nHeaderHeight - ::nGrpHeight - 10 //ROW_EXTRAHEIGHT
      endif
      cHeader  := ::cGrpHdr
      nHeight  := ::nGrpHeight
      if Val( Right( FWVERSION , 5 ) ) > 14.06
         oFont    := ::GrpFont   //AL_CENTER
      else
         oFont    := ::oGrpFont  //AL_CENTER
      endif
      nStyle   := ::DefStyle( ::nHGrpAlign, ! ( CRLF $ cHeader ) )
      if Empty( aBitmap )
         nBmpNo         := ::nGrpBmpNo
         aBitmap     := ::aBitmap( nBmpNo )
      endif
      nBmpAlign   := IF( ::nHGrpAlign = AL_LEFT, AL_RIGHT, AL_LEFT )
   endif

   if ::oBrw:l2007
      //if empty( ::bClrHeader )                                 // A�adido CNL - 18/02/2013
      GradientFill( hDC, nRow - 1, nCol, nRow + nHeight - 1, nCol + nWidth, ;
                       Eval( ::bClrGrad, lInvert ) )
      //else                                                     // A�adido CNL - 18/02/2013
      //   hBrush  := CreateSolidBrush( aColors[ 2 ] )
      //   FillRect( hDC, { nRow, nCol, nRow + nHeight, nCol + nWidth }, hBrush )
      //   DeleteObject( hBrush )
      //endif
   else
      hBrush  := CreateSolidBrush( aColors[ 2 ] )       //OJO con nRow-1 y nWidth+1   06/06/2013
      FillRect( hDC, { nRow - 2, nCol-1, nRow + nHeight+1, nCol + nWidth + 2}, hBrush )  //18/08/2013
      if ::nPos = 1
         if ::oBrw:lRecordSelector
         FillRect( hDC, { 0, 0, nRow + nHeight+1, ::oBrw:nRecSelWidth - 2}, hBrush )
         endif
      endif
      DeleteObject( hBrush )
   endif

   if !Empty( nRowFill )
      nRow := nRowFill
   endif

   nCol    += ( COL_EXTRAWIDTH / 2 )
   nWidth  -=  COL_EXTRAWIDTH
   nRow    += ( ROW_EXTRAHEIGHT / 2 )
   nHeight -=  ROW_EXTRAHEIGHT
/*
   if ( ValType( ::cOrder ) == 'C' .and. ::cOrder $ 'AD' ) .or. ;       // 2008-07-16
      ( nBmpNo > 0 .and. nBmpNo <= Len( ::aBitmaps ) )

      if ! Empty( ::cOrder )
         aBitmap     := ::oBrw:aSortBmp[ If( ::cOrder == 'A', 1, 2 ) ]
         nBmpAlign   := AL_RIGHT
      else
         aBitmap     := ::aBitmaps[ nBmpNo ]
         nBmpAlign   := ::nHeadBmpAlign
      endif
*/
   if ! Empty( aBitmap )

      nWidth         -= aBitmap[ BITMAP_WIDTH ]
      if !::lSetBmpCol                        // A�adido por Cnl : 16/02/2005

         if Empty(cHeader)
            nBmpCol := nCol + nWidth / 2
         elseif nBmpAlign == AL_LEFT
            nBmpCol     := nCol
            nCol        += aBitmap[ BITMAP_WIDTH ] + BMP_EXTRAWIDTH
         else
            nBmpCol := nCol + nWidth
         endif
      else                                    // A�adido por Cnl : 16/02/2005
         if ::nPos = 1
            if Empty(cHeader)
               //nBmpCol := nCol + ( ( nWidth - aBitmap[ BITMAP_WIDTH ] ) / 2 )
               nBmpCol := ::oBrw:nLeft + ( COL_EXTRAWIDTH / 2 )
            elseif ::nHeadBmpAlign == AL_LEFT
               nBmpCol := ::oBrw:nLeft + ( COL_EXTRAWIDTH / 2 )
               //nBmpCol := nCol
               //nCol    += aBitmap[ BITMAP_WIDTH ] + BMP_EXTRAWIDTH
            else
               nBmpCol := ::oBrw:nLeft + ( COL_EXTRAWIDTH / 2 )
               //nBmpCol := nCol + nWidth
            endif
         endif
      endif

      nWidth         -= BMP_EXTRAWIDTH
      nBmpRow        := nRow + ( nHeight - aBitmap[ BITMAP_HEIGHT ] ) / 2 //+ If( nGrpWidth == nil, 4, 0 ) // commeneted out on apr 20,2010
      if SetAlpha() .and. aBitmap[ BITMAP_ALPHA ]
         ABPaint( hDC, nBmpCol, nBmpRow, aBitmap[ BITMAP_HANDLE ], ::nAlphaLevelHeader )
      elseif ::oBrw:l2007
         DEFAULT aBitmap[ BITMAP_ZEROCLR ] := GetZeroZeroClr( hDC, aBitmap[ BITMAP_HANDLE ] )
         SetBkColor( hDC, nRGB( 255, 255, 255 ) )
         TransBmp( aBitmap[ BITMAP_HANDLE ], aBitmap[ BITMAP_WIDTH ], aBitmap[ BITMAP_HEIGHT ],;
                   aBitmap[ BITMAP_ZEROCLR ], hDC, nBmpCol, nBmpRow, nBmpWidth( aBitmap[ BITMAP_HANDLE ] ),;
                   nBmpHeight( aBitmap[ BITMAP_HANDLE ] ) )
      else
         PalBmpDraw( hDC, nBmpRow, nBmpCol,;
                     aBitmap[ BITMAP_HANDLE ],;
                     aBitmap[ BITMAP_PALETTE ],;
                     aBitmap[ BITMAP_WIDTH ],;
                     aBitmap[ BITMAP_HEIGHT ];
                     ,, .t., aColors[ 2 ] )
      endif
   endif

   if Empty( cHeader )
      ::oBrw:ReleaseDC()
      return nil
   endif
   if !empty( oFont )
      oFont:Activate( hDC )
   endif
   SetTextColor( hDC, aColors[ 1 ] )
   SetBkColor( hDC, aColors[ 2 ] )
   SetBkMode ( hDC, 1 ) // transparent

   if FontEsc( oFont ) % 3600 == 900

      nBottom  := nRow + nHeight / 2
      nBottom  += ( ::oBrw:GetWidth( cHeader, ::oHeaderFont ) / 2 )
      nCol     := nCol + nWidth / 2
      nCol     -= FontHeight( ::oBrw, ::oHeaderFont ) / 2

      DrawTextEx( hDC, cHeader,;
                  { nBottom, nCol, nRow, nCol + nWidth }, ;
                  DT_LEFT + DT_VCENTER )

   elseif FontEsc( oFont ) % 3600  == 2700

      nBottom  := nRow + nHeight / 2
      nBottom  += ( ::oBrw:GetWidth( cHeader, ::oHeaderFont ) / 2 )
      nCol     := nCol + nWidth / 2
      nCol     += FontHeight( ::oBrw, ::oHeaderFont ) / 2

      DrawTextEx( hDC, cHeader,;
                  { nRow, nCol, nBottom, nCol - nWidth / 2 }, ;
                  DT_LEFT + DT_VCENTER )

   else
      nBottom  := nRow + nHeight
      DrawTextEx( hDC, cHeader, { nRow, nCol, nBottom, nCol + Min( nWidth, ::oBrw:nWidth - 50 ) }, nStyle )
   endif
   if !empty( oFont )
      oFont:Deactivate( hDC )
   endif

   if !lOwnDC
      ::oBrw:ReleaseDC()
   endif

return nil

//----------------------------------------------------------------------------//

METHOD PaintFooter( nRow, nCol, nHeight, lInvert ) CLASS TXCBrwColumn

   local hDC, hBrush
   local oFont
   local aColors, aBitmap
   local cFooter
   local nWidth, nBmpRow, nBmpCol, nBmpNo, nBottom

   DEFAULT lInvert := .f.

   if nCol != nil
      ::nDisplayCol := nCol
   else
      nCol := ::nDisplayCol
   endif

   if !lInvert
      aColors := Eval( ::bClrFooter )
   else
      aColors := { CLR_WHITE, CLR_BLUE }
   endif

   hDC     := ::oBrw:GetDC()
   if Val( Right( FWVERSION , 5 ) ) > 14.06
      oFont   := ::FooterFont
   else
      oFont   := ::oFooterFont
   endif
   cFooter := ::footerStr()
   nWidth  := ::nWidth
   nBmpNo  := ::nFootBmpNo

   nBottom = nRow + ( nHeight / 3 )
   if ::oBrw:l2007
      GradientFill( hDC, nRow - 1, nCol, nRow + nHeight - 1, nCol + nWidth, ;
            Eval( ::bClrGrad, lInvert ) )

   else
      hBrush  := CreateColorBrush( aColors[ 2 ] )
      // Esto habia 18/08/2013
      //FillRect( hDC, {nRow, nCol, nRow + nHeight, nCol + nWidth}, hBrush )
      FillRect( hDC, { nRow-1, nCol, nRow + nHeight + IF(::oBrw:lHSCroll,0,1),;
                       nCol + nWidth + 2 }, hBrush )
      if ::nPos = 1
         if ::oBrw:lRecordSelector
         FillRect( hDC, { nRow-1, 0, nRow + nHeight+ IF(::oBrw:lHSCroll,0,1),;
                          ::oBrw:nRecSelWidth - 2}, hBrush )
         endif
      endif

      DeleteObject( hBrush )
   endif

   nCol    += ( COL_EXTRAWIDTH / 2 )
   nWidth  -=  COL_EXTRAWIDTH
   nRow    += ( ROW_EXTRAHEIGHT / 2 )
   nHeight -=  ROW_EXTRAHEIGHT

   if !Empty( aBitmap := ::aBitmap( nBmpNo ) )
      nWidth  -= aBitmap[ BITMAP_WIDTH ]
      if Empty(cFooter)
         nBmpCol := nCol + nWidth / 2
      elseif ::nFootBmpAlign == AL_LEFT
         nBmpCol := nCol
         nCol    += aBitmap[ BITMAP_WIDTH ] + BMP_EXTRAWIDTH
      else
         nBmpCol := nCol + nWidth
      endif
      nWidth  -= BMP_EXTRAWIDTH
      nBmpRow := nRow + ( nHeight - aBitmap[ BITMAP_HEIGHT ] ) / 2 + 2
      if ! ::oBrw:l2007
         if SetAlpha() .and. aBitmap[ BITMAP_ALPHA ]
            ABPaint( hDC, nBmpCol, nBmpRow, aBitmap[ BITMAP_HANDLE ], ::nAlphaLevelFooter )
         else

            PalBmpDraw( hDC, nBmpRow, nBmpCol,;
                        aBitmap[ BITMAP_HANDLE ],;
                        aBitmap[ BITMAP_PALETTE ],;
                        aBitmap[ BITMAP_WIDTH ],;
                        aBitmap[ BITMAP_HEIGHT ];
                        ,, .t., aColors[ 2 ] )
         endif
      else
         if SetAlpha() .and. aBitmap[ BITMAP_ALPHA ]
            ABPaint( hDC, nBmpCol, nBmpRow, aBitmap[ BITMAP_HANDLE ], ::nAlphaLevelFooter )
         else

            DEFAULT aBitmap[ BITMAP_ZEROCLR ] := GetZeroZeroClr( hDC, aBitmap[ BITMAP_HANDLE ] )
            SetBkColor( hDC, nRGB( 255, 255, 255 ) )
            TransBmp( aBitmap[ BITMAP_HANDLE ], aBitmap[ BITMAP_WIDTH ], aBitmap[ BITMAP_HEIGHT ],;
                      aBitmap[ BITMAP_ZEROCLR ], hDC, nBmpCol, nBmpRow, nBmpWidth( aBitmap[ BITMAP_HANDLE ] ),;
                      nBmpHeight( aBitmap[ BITMAP_HANDLE ] ) )
         endif
      endif
   endif

   if Empty( cFooter )
      ::oBrw:ReleaseDC()
      return nil
   endif
   if !empty( oFont )
      oFont:Activate( hDC )
   endif
   SetTextColor( hDC, aColors[ 1 ] )
   SetBkColor( hDC, aColors[ 2 ] )
   SetBkMode ( hDC, 1 ) // transparent
   DrawTextEx( hDC, cFooter,;
               {nRow, nCol, nRow + nHeight, nCol + nWidth},;
               ::nFootStyle )
   if !empty( oFont )
      oFont:Deactivate( hDC )
   endif

   ::oBrw:ReleaseDC()

return nil

//----------------------------------------------------------------------------//

METHOD PaintCell( nRow, nCol, nHeight, lHighLite, lSelected, nOrder, nPaintRow ) CLASS TXCBrwColumn

   local hDC, oBrush, hBrush, nOldColor, hBmp
   local oBrush1, oBrush2, hBrush1, hBrush2, aColor2, nWidth1
   local oFont
   local aColors, aBitmap, aBmpPal
   local cData, nTxtHeight, aRect
   local nWidth, nTop, nBottom, nBmpRow, nBmpCol, nBmpNo, nButtonRow, nButtonCol,nBtnWidth,;
         nRectWidth, nRectCol, nStyle, nType, nIndent, nBtnBmp, nFontHeight
   local lTransparent := .f.
   local lStretch     := .f.
   local lBrush       := .f.
   local cImagen, nBmpW, nBmpH

   DEFAULT lHighLite := .f.,;
           lSelected := .f.,;
           nOrder    := 0

   nBtnBmp := 0

   if ( ::oEditGet != nil .and. nPaintRow == ::oBrw:nRowSel ) .or. ::oEditLbx != nil .or. ::oBrw:nLen == 0
      //if !::oBrw:lRowDividerComplete
         return nil
      //endif
   endif


   if nCol != nil
      ::nDisplayCol := nCol
   else
      nCol := ::nDisplayCol
   endif

   if ::bStrData != nil //.and. !::hChecked
      cData := Eval( ::bStrData )
      if ValType( cData ) != 'C'
         cData := cValToChar( cData )
      endif
      if ! Empty( ::nDataStrAlign )
         cData := AllTrim( cData )
      endif
      if isrtf( cData )
         cData := "<RichText>"
      elseif isGtf( cData )
         cData := GtfToTxt( cData )
      endif
   else
      cData := ""
   endif

   if ::bBmpData != nil
      nBmpNo := Eval( ::bBmpData, ::Value() )
   else
      nBmpNo := 0
   endif

   if lHighLite
      if ::oBrw:hWnd == GetFocus()
         if lSelected
            if nOrder == ::oBrw:nColSel
               aColors  := Eval( ::bClrSelFocus )  // Eval( ::oBrw:bClrSelFocus )
            else
               aColors := Eval( If( ::oBrw:bClrRowFocus != nil, ::oBrw:bClrRowFocus, ::bClrSelFocus ) )
            endif
          else
            aColors := Eval( If( ::oBrw:bClrRowFocus != nil, ::oBrw:bClrRowFocus, ::bClrSelFocus ) )
          endif
      else
         aColors := Eval( ::bClrSel )
      endif
   else
      aColors := Eval( ::bClrStd )
      lTransparent := ::oBrw:lTransparent
   endif

   hDC     := ::oBrw:GetDC()
   if Val( Right( FWVERSION , 5 ) ) > 14.06
      oFont   := ::DataFont
   else
      oFont   := ::oDataFont
   endif
   if ValType( oFont ) == "B"
      oFont = Eval( oFont, Self )
   endif
   nWidth  := ::nWidth

   if ::oBrush != nil
      if ValType( ::oBrush ) == "B"
         oBrush   := Eval( ::oBrush, Self )
      else
         oBrush   := ::oBrush
      endif
   endif

   if oBrush != nil
      hBrush      := oBrush:hBrush
      lBrush      := .t.
      lTransparent:= .f.
   elseif  ! IfNil( ::lColTransparent, lTransparent )  //!lTransparent .and. !::lColTransparent
      hBrush  := CreateColorBrush( aColors[ 2 ] )
   elseif ::lColTransparent == .T.
         hBrush := CreateColorBrush( 0 )
         lTransparent := .t.
   endif

   nStyle  := ::oBrw:nColDividerStyle
   nType   := ::nEditType

   if nStyle == 0
      nRectWidth := nWidth + 2
      nRectCol   := nCol
   elseif nStyle < 5 .and. nOrder > 1
      nRectWidth := nWidth + 1
      nRectCol   := nCol - 1
   else
      nRectWidth := nWidth
      nRectCol   := nCol
   endif

   if Empty( nHeight )      // A�adido 31/08/2013 al poner el SetColsAsRows( {1,2} )
      nHeight := nWidth
   endif

   nBottom  := nRow + nHeight
   if ! lTransparent
      nTop        := nRow
      if ::lMergeVert .and. lHighLite .and. lSelected .and. nOrder == ::oBrw:nColSel
         ::MergeArea( @nTop, @nBottom, nPaintRow )
      endif
      if ValType( aColors[ 2 ] ) == 'A'
         GradientFill( hDC, nTop, nRectCol, nBottom-1, Min( nRectCol + nRectWidth, ::oBrw:BrwWidth() ), aColors[ 2 ], .t. )
      else
         FillRect( hDC, { nTop, nRectCol, nBottom, Min( nRectCol + nRectWidth, ::oBrw:BrwWidth() ) }, hBrush )
      endif
   endif

   if ::bIndent != nil
      nIndent  := Eval( ::bIndent, Self )
      if ! Empty( nIndent )
         nCol   += nIndent
         nWidth -= nIndent
      endif
   endif

   nCol    += ( COL_EXTRAWIDTH / 2 )
   nWidth  -=  COL_EXTRAWIDTH
   nRow    += ( ROW_EXTRAHEIGHT / 2 )
   nHeight -=  ROW_EXTRAHEIGHT

//   if nType > 1
//      nButtonRow := nRow
//      nButtonCol := nCol + nWidth - 10
//      nWidth -= 15
//   endif

   if nType > 1
      if ! Empty( aBitmap := ::aBitmap( ::nBtnBmp ) )
         nBtnWidth      := aBitMap[ BITMAP_WIDTH ] + 1
         aBitmap        := nil
      else
         nBtnWidth      :=    10
      endif
      nButtonRow := nRow
      nButtonCol := nCol + nWidth - nBtnWidth
      nWidth -= ( nBtnWidth + 5 )
   else
      if ::lWillShowABtn // to avoid the "dancing" of column data to the left
         nWidth -= 15
      endif
   endif

   if ::lProgBar
      aColor2  := Eval( ::bClrProg )
      hBrush1  := CreateColorBrush( aColor2[ 1 ] )
      hBrush2  := CreateColorBrush( aColor2[ 2 ] )
      nWidth1  := Min( ::Value() * nWidth / ;
         Max( 1, If( ValType( ::nProgTot ) == 'B', Eval( ::nProgTot, Self ), ::nProgTot ) ), ;
         nWidth )

      FillRect( hDC, { nRow, nCol, nRow + nHeight, Min( nCol + nWidth1, ::oBrw:BrwWidth() - 4 ) }, hBrush1 )

      if nCol + nWidth1 < ::oBrw:BrwWidth() - 4
         FillRect( hDC, { nRow, nCol + nWidth1 + 1, nRow + nHeight, ;
             Min( nCol + nWidth, ::oBrw:BrwWidth() - 4 ) }, hBrush2 )
      endif
      DeleteObject( hBrush1 )
      DeleteObject( hBrush2 )

   endif

   if !Empty( aBitmap := ::aBitmap( nBmpNo ) )
      nWidth  -= aBitmap[ BITMAP_WIDTH ]
      if ::bStrData == nil .OR. ::nDataBmpAlign == AL_CENTER // Align Imagen SetCheck
         nBmpCol  := Max( 0, nCol + nWidth / 2 )
         lStretch := ::lBmpStretch

      elseif ::nDataBmpAlign == AL_LEFT
         nBmpCol := nCol
         nCol    += aBitmap[ BITMAP_WIDTH ] + BMP_EXTRAWIDTH
      else
         nBmpCol := nCol + nWidth
      endif
      nWidth  -= BMP_EXTRAWIDTH
      nBmpRow := nRow + ( ( nHeight - aBitmap[ BITMAP_HEIGHT ] ) / 2 )

      if ::lMergeVert
         nTop     := nRow
         nBottom  := nRow + nHeight - 1
         ::MergeArea( @nTop, @nBottom, nPaintRow )
         nBmpRow := nTop + ( ( ( nBottom - nTop + 1 ) - aBitmap[ BITMAP_HEIGHT ] ) / 2 )
      endif
      // Paint bitmaps
      /*DEFAULT*/ aBitmap[ BITMAP_ZEROCLR ] := GetZeroZeroClr( hDC, aBitmap[ BITMAP_HANDLE ] )

      if lStretch

         if SetAlpha() .and. aBitmap[ BITMAP_ALPHA ]
            hBmp := ResizeImg( aBitmap[ BITMAP_HANDLE ], Min( nRectWidth,::oBrw:BrwWidth() - nRectCol - 4 ), nBottom - nRow )
            ABPaint( hDC, nRectCol, nRow, hBmp, ::nAlphaLevel() )
         else
            nOldColor  := SetBkColor( hDC, nRGB( 255, 255, 255 ) )
            TransBmp( aBitmap[ BITMAP_HANDLE ], aBitmap[ BITMAP_WIDTH ], aBitmap[ BITMAP_HEIGHT ],;
                      aBitmap[ BITMAP_ZEROCLR ], hDC, nRectCol, nRow, Min( nRectWidth,::oBrw:BrwWidth() - nRectCol - 4 ), ;
                      nBottom - nRow )
            SetBkColor( hDC, nOldcolor )
         endif

      else
         if SetAlpha() .and. aBitmap[ BITMAP_ALPHA ]
            ABPaint( hDC, nBmpCol, nBmpRow,aBitmap[ BITMAP_HANDLE ], ::nAlphaLevel() )
         else
            if ::oBrw:lTransparent .or. ValType( aColors[ 2 ] ) == 'A' // transparent bitmaps with brush
                nOldColor := SetBkColor( hDC, nRGB( 255, 255, 255 ) )

                TransBmp( aBitmap[ BITMAP_HANDLE ], aBitmap[ BITMAP_WIDTH ], aBitmap[ BITMAP_HEIGHT ],;
                         aBitmap[ BITMAP_ZEROCLR ], hDC, nBmpCol, nBmpRow, aBitmap[ BITMAP_WIDTH ], ;
                         aBitmap[ BITMAP_HEIGHT ] )

                SetBkColor( hDC, nOldColor )
             else
                PalBmpDraw( hDC, nBmpRow, nBmpCol,;
                           aBitmap[ BITMAP_HANDLE ],;
                           aBitmap[ BITMAP_PALETTE ],;
                           aBitmap[ BITMAP_WIDTH ],;
                           aBitmap[ BITMAP_HEIGHT ];
                           ,, ::lBmpTransparent, aColors[ 2 ] )

            endif
         endif
      endif
   endif

   if ! Empty( cData ) .and. IsBinaryData( cData )
      if IfNil( FITypeFromMemory( cData ), -1 ) >= 0
         cImagen  := cData
         cData    := ''
      else
         cData    := '<binary>'
      endif
   endif

   if ! Empty( cImagen ) .or. ::cDataType $ "FP"   // IMAGE
      if ! Empty( cImagen )
         hBmp     := FILoadFromMemory( cImagen )
      else
         if ::bStrImage == NIL
            cImagen := ::Value()
         else
            cImagen := Eval( ::bStrImage, Self, ::oBrw )
         endif
         if ::cDataType == 'F' .and. File( cImagen )
            hBmp     := FILoadImg( cImagen )
         else
            hBmp     := FILoadFromMemory( IfNil( cImagen, '' ) )
         endif
      endif

      aBmpPal     := { hBmp, 0 }
      if aBmpPal[ BITMAP_HANDLE ] == 0
         aBmpPal := PalBmpLoad( cImagen )
      endif

      if aBmpPal[ BITMAP_HANDLE ] != 0
         Aadd(aBmpPal, nBmpWidth( aBmpPal[ BITMAP_HANDLE ] ) )
         Aadd(aBmpPal, nBmpHeight( aBmpPal[ BITMAP_HANDLE ] ) )
         Aadd(aBmpPal, if ( ::lBmpTransparent, GetZeroZeroClr( hDC, aBmpPal[ BITMAP_HANDLE ] ),0) )
         Aadd(aBmpPal, HasAlpha( aBmpPal[ BITMAP_HANDLE ] ) )

         if ::lBmpStretch
            nBmpW       := nWidth - 2
            nBmpH       := nBottom - nRow - 2
            nBmpCol     := nCol + 1
            nBmpRow     := nRow + 1
         else
            nBmpW       := aBmpPal[ BITMAP_WIDTH ]
            nBmpH       := aBmpPal[ BITMAP_HEIGHT ]
            if nBmpW > ( nWidth - 4 )
               nBmpH    *= ( ( nWidth - 4 ) / nBmpW )
               nBmpW    := nWidth - 4
            endif
            if nBmpH > ( nBottom - nRow - 4 )
               nBmpW    *= ( ( nBottom - nRow - 4 ) / nBmpH )
               nBmpH    := ( nBottom - nRow ) - 4
            endif
            nBmpRow     := nRow + ( nHeight - nBmpH ) / 2 + 2
            nBmpCol     := nCol + 2

            if ::nDataBmpAlign == AL_CENTER
               nBmpCol  := nCol + ( nWidth - nBmpW ) / 2
            elseif ::nDataBmpAlign == AL_RIGHT
               nBmpCol  := nCol + nWidth - nBmpW
            endif

         endif

         if SetAlpha() .and. aBmpPal[ BITMAP_ALPHA ]

            hBmp := ResizeImg( aBmpPal[ BITMAP_HANDLE ], nBmpW, nBmpH )
            ABPaint( hDC, nBmpCol, nBmpRow, hBmp, ::nAlphaLevel() )
            DeleteObject( hBmp )

         elseif ::lBmpTransparent

            nOldColor := SetBkColor( hDC, nRGB( 255, 255, 255 ) )
            TransBmp( aBmpPal[ BITMAP_HANDLE ], aBmpPal[ BITMAP_WIDTH ], aBmpPal[ BITMAP_HEIGHT ],;
                      aBmpPal[ BITMAP_ZEROCLR ], hDC, nBmpCol, nBmpRow, nBmpW, nBmpH )
            SetBkColor( hDC, nOldColor )

         else

            if nBmpW != aBmpPal[ BITMAP_WIDTH ] .or. nBmpH != aBmpPal[ BITMAP_HEIGHT ]
               hBmp := ResizeImg( aBmpPal[ BITMAP_HANDLE ], nBmpW, nBmpH )
               DrawBitmap( hDC, hBmp, nBmpRow, nBmpCol )
               DeleteObject( hBmp )
            else
               DrawBitmap( hDC, aBmpPal[ BITMAP_HANDLE ], nBmpRow, nBmpCol )
            endif

         endif

      endif

   endif

   if ! Empty( cData ) .and. ! ( ::cDataType $ "PF" ) //.and. nType >= 0
      if !empty( oFont )
      oFont:Activate( hDC )
      endif
      nFontHeight := GetTextHeight( ::oBrw:hWnd, hDC )
      if ::oBrw:lTransparent .and. ::oBrw:lContrastClr
         SetTextColor( hDC, ContrastColor( hDC, nCol, nRow, ;
            Min( nWidth, ::oBrw:BrwWidth() - nCol ), ;
            nHeight, aColors[ 1 ] ) )
      else
         SetTextColor( hDC, aColors[ 1 ] )
      endif
      if lTransparent .or. lBrush .or. ::lProgBar .or. ValType( aColors[ 2 ] ) == 'A'
         SetBkMode( hDC, 1 )
      else
         nOldColor := SetBkColor( hDC, aColors[ 2 ] )
      endif

      nTop     := nRow
      nBottom  := nRow + nHeight

      if ::lMergeVert
         ::MergeArea( @nTop, @nBottom, nPaintRow )
      endif

      aRect       := { nTop, nCol, nBottom, Min( nCol + nWidth, ::oBrw:BrwWidth() - 5 ) }
      if ::bPaintText == nil

         nStyle      := ::nDataStyle
         if ::oBrw:nDataType == DATATYPE_ARRAY .and. ;
            ::nDataStrAlign == AL_LEFT .and. ;
            ValType( ::Value ) $ 'ND'

            nStyle   := ::DefStyle( AL_RIGHT, .t. )
         endif
/*
         if ::cDataType == 'M' .and. ::nDataLines == nil
            if ::oBrw:nDataLines > 1
               ::nDataLines   := ::oBrw:nDataLines
            elseif ( ::oBrw:nRowHeight >= 2 * nFontHeight + 4 )
               ::nDataLines   := 2
               ::nDataStyle   := ;
               nStyle         := ::DefStyle( ::nDataStrAlign, .f. )
            endif
         endif
*/
         if ::cDataType != nil .and. ::cDataType $ 'CM' .and. lAnd( nStyle, DT_SINGLELINE ) .and. ;
            ::oBrw:nRowHeight > 2 * nFontHeight + 2

            cData    := Trim( cData )
            if ( nTxtHeight := DrawTextEx( hDC, cData, aRect, nOr( DT_CALCRECT, DT_WORDBREAK ) ) ) > ;
                              DrawTextEx( hDC, cData, aRect, nOr( DT_CALCRECT, DT_SINGLELINE ) )

               nStyle      := nOr( nAnd( nStyle, nNot( DT_SINGLELINE ) ), DT_WORDBREAK )
               aRect[ 1 ]  += Max( 0, Int( ( nHeight - nTxtHeight ) / 2 ) )

               if ::nDataLines == nil .and. ::cDataType == 'M'
                  ::nDataLines   := 2
               endif

            endif
         endif
         DrawTextEx( hDC, cData, aRect, nStyle )
      else
         Eval( ::bPaintText, Self, hDC, cData, aRect, aColors, lHighLite )
      endif
      if nOldColor != nil
         SetBkcolor( hDC, nOldColor )
         nOldColor := nil
      endif
      if !empty( oFont )
         oFont:Deactivate( hDC )
      endif
   else
      aRect       := { nRow, nCol, nRow + nHeight, Min( nCol + nWidth, ::oBrw:BrwWidth() - 5 ) }
      if ::bPaintText != nil
         Eval( ::bPaintText, Self, hDC, "", aRect, aColors, lHighLite )
      endif
   endif

   if nType > 1 .and. nType < EDIT_DATE
      if lSelected
         if !::lBtnTransparent
               WndBoxRaised(hDC, nButtonRow -1 , nButtonCol - 1,;
                                 nButtonRow + nHeight, nButtonCol + nBtnWidth + 1 ) // ButtonGet
         endif

         if nType == EDIT_LISTBOX .or. nType == EDIT_GET_LISTBOX
           ::oBtnElip:Hide()
           ::oBtnList:Move( nButtonRow, nButtonCol, nBtnWidth + 1, nHeight, .f.) // ButtonGet
           ::oBtnList:Show()
           ::oBtnList:GetDC()
           if ::lBtnTransparent
              ::oBtnList:SetColor( aColors[ 1 ],aColors[ 2 ] )
           else
              FillRect( hDC, {nButtonRow, nButtonCol, nButtonRow + nHeight , nButtonCol + nBtnWidth + 1 } ,;   // ButtonGet
                       ::oBtnList:oBrush:hBrush  )
           endif
           ::oBtnList:Paint()
           ::oBtnList:ReleaseDC()
         else
            ::oBtnList:Hide()
            ::oBtnElip:Move( nButtonRow, nButtonCol, nBtnWidth + 1, nHeight, .f.) // ButtonGet
            ::oBtnElip:Show()
            ::oBtnElip:GetDC()
           if ::lBtnTransparent
              ::oBtnElip:SetColor( aColors[ 1 ],aColors[ 2 ] )
           else
              FillRect( hDC, {nButtonRow, nButtonCol, nButtonRow + nHeight , nButtonCol + nBtnWidth + 1 },; // ButtonGet
                      ::oBtnElip:oBrush:hBrush )
           endif
            ::oBtnElip:Paint()
            ::oBtnElip:ReleaseDC()
         endif
      endif
   endif

   //if ::cDataType  = 'O'
   // if ::lControlUser
      if empty( ::oControlUser )
   //      if !empty( ::bControlUser ) .and. Valtype( ::bControlUser ) = "B"
   //         // Creacion del Control definido en codeblock
   //         ::oControlUser := Eval( ::oControlUser, oCol )
   //      else
           /*
   //      // Creacion de Control por defecto "TPanel" �?
            ::oControlUser := TPanel():New( nTop, nCol-2, ::oBrw:nRowHeight + nTop - 4,;
                                           ::nWidth , ::oBrw )
            if ::nWidth > ::oBrw:nWidth //- 24
            ::oControlUser:nStyle  = nOr( ::oControlUser:nStyle, WS_HSCROLL )
            @ ::oControlUser:nBottom - 24, 1 SCROLLBAR ::oControlUser:oVScroll ;
              HORIZONTAL RANGE 0, ::nWidth SIZE ::oBrw:nWidth-24, 24 ;
              PIXEL OF ::oControlUser
            endif
            */
            /*
            ::oControlUser := TDialog():New( nTop, nCol-2, ;
                                       ::oBrw:nRowHeight + nTop - 4,;
                                       ::nWidth ,"Hola",,,,,METRO_GRIS2,METRO_WINDOW,, ::oBrw, .T. )
            */
            //::oControlUser:Hide()
            //::oControlUser:nStyle -= WS_CAPTION

            //::oControlUser:SetColor( METRO_GRIS1 , METRO_WINDOW ) //GRIS1 )
   //      endif
         //::lCtrlCreated  := .T.
   //   endif
      else
   //   if empty( ::oControlUser )
   //      if !empty( ::bControlUser ) .and. Valtype( ::bControlUser ) = "B"
   //
   //      endif
   //      ::lCtrlCreated  := .T.
   //   endif
      endif
   //endif

   if ::lCtrlCreated
   //   if !empty( ::oControlUser ) .and. ValType( ::oControlUser ) = "O"
   //      ::oBrw:nRowHeight , ::nWidth
         //::oControlUser:Paint()
         //::oControlUSer:Refresh()
   //   endif
   endif

   if hBrush != nil .and. ! lBrush
      DeleteObject( hBrush )
   endif
   if aBmpPal != nil
      DeleteObject( aBmpPal[ BITMAP_HANDLE ]  )
   endif
   ::oBrw:ReleaseDC()

return nil

//----------------------------------------------------------------------------//
/*
METHOD PaintData( nRow, nCol, nHeight, lHighLite, lSelected, nOrder, nPaintRow ) CLASS TXCBrwColumn


return nil
*/
//----------------------------------------------------------------------------//

METHOD PintaLineasHV( nTop, nCol, nBottom )  CLASS TXCBrwColumn
Local hDC
Local aCoors := {,,,}
Local nDesp  := 0
Local hPen

//aCoors      := { nTop, nCol, nBottom, Min( nCol + ::nWidth, ::oBrw:BrwWidth() - 5 ) }
//hDC         := ::oBrw:GetDC()
//::oBrw:ReleaseDC()

   hPen := ::CalcClrPen( 1 )
   if !Empty( ::aBitmaps )
      if ::lLinHT
       MoveTo( hDC, aCoors[ 2 ], aCoors[ 1 ]+nDesp )
       LineTo( hDC, aCoors[ 4 ]+::nWidth-IF(::lLinVR,0,1), aCoors[ 1 ]+nDesp, hPen )
      endif
      if ::lLinHB
       MoveTo( hDC, aCoors[ 2 ], aCoors[ 3 ]-nDesp )
       LineTo( hDC, aCoors[ 4 ]+::nWidth-IF(::lLinVR,0,1), aCoors[ 3 ]-nDesp, hPen )
      endif
      if ::lLinVL
       MoveTo( hDC, aCoors[ 2 ]+nDesp, aCoors[ 1 ]+IF( Empty( nDesp), 0, 1 ) )
       LineTo( hDC, aCoors[ 2 ]+nDesp, aCoors[ 1 ]+::oBrw:nRowHeight-4-IF( Empty( nDesp), 0, 1 ), hPen )
      endif
      if ::lLinVR
       MoveTo( hDC, aCoors[ 2 ]-nDesp+::nWidth-6, aCoors[ 1 ]+IF( Empty( nDesp), 0, 1 ) )
       LineTo( hDC, aCoors[ 2 ]-nDesp+::nWidth-6, aCoors[ 1 ]+::oBrw:nRowHeight-4-+IF( Empty( nDesp), 0, 1 ), hPen )
      endif
   endif

Return nil

//----------------------------------------------------------------------------//

METHOD CalcClrPen( nT )   CLASS TXCBrwColumn
Local hDC
Local nColor
Local lEnBmp  := .F.
Local hPen
Local aColor  := { 16777215, 12632256, 8421504, 4210752, 0 }
Local aCRgb   := { 0, 64, 128, 192 , 255 }
Local nCSel   := 0
Local n       := 0
Local aCoors  := {,,,}

DEFAULT    nT := 0
hDC           := ::oBrw:GetDC()

   nColor  := GetPixel( hDC, aCoors[ 2 ] + 4 , aCoors[ 1 ] + 4 )
   if lEnBmp
      For n = Len( aColor ) to 1 step -1
          if nColor <= aColor[ n ]
             nCSel  := aCRgb[ n ]
             n := 0
          endif
      Next n
   else
      if nColor < aColor[ 3 ]
         nCSel := aColor[ 1 ]
         hPen  := ::oBrw:hWhitePen
      else
         nCSel := aColor[ 5 ]
         hPen  := ::oBrw:hRowPen
      endif
   endif

Return IF( Empty( nT ), nCSel, hPen )

//----------------------------------------------------------------------------//
/*
METHOD Edit( nKey ) CLASS TXCBrwColumn


return .t.
*/

//-------------------------------------------------------------------//

/*
METHOD DrawTxtRow( oCol, hDC, cText, aCoors , cFont, nHFont, aItems, lEnBmp,;
                  lLinHT, lLinHB, lLinVL, lLinVR ) CLASS TXCBrwColumn

return nColor
*/
   /*
   hFontt = CreateFont( { nHFont, 0, ::nEscapement,;
                         ::nOrientation, ::nWeight, ::lItalic,;
                         ::lUnderLine, ::lStrikeOut, ::nCharSet,;
                         ::nOutPrecision, ::nClipPrecision,;
                         ::nQuality, ::nPitchFamily, ::cFaceName } )

   */
//----------------------------------------------------------------------------//




/*
METHOD PaintData( nRow, nCol, nHeight, lHighLite, lSelected, nOrder, nPaintRow ) CLASS TXCBrwColumn
Return ::Super:PaintData( nRow, nCol, nHeight, lHighLite, lSelected, nOrder, nPaintRow )
*/

//----------------------------------------------------------------------------//

/*
STATIC FUNCTION DRAWTEXT( oCol, hDC, oBrw1, cText, aCoord, oBold, oItalic )

RETURN NIL
*/
//----------------------------------------------------------------------------//
/*
static function EditGetLostFocus( oGet, hWndFocus, oBrw, oEditGet, oCol )


return nil

//----------------------------------------------------------------------------//

static function EditGetkeyDown( Self, nKey )


return nil
*/
//----------------------------------------------------------------------------//


static function CreateColorBrush( uClr )

   if ValType( uClr ) == 'A'
      uClr     := uClr[ 1 ][ 2 ]
   endif

return CreateSolidBrush( uClr )

//------------------------------------------------------------------//

static function GetZeroZeroClr( hDC, hBmp )

    local hDCMem, hOldBmp, nZeroZeroClr

    hDCMem = CreateCompatibleDC( hDC )
    hOldBmp = SelectObject( hDCMem, hBmp )
    nZeroZeroClr = GetPixel( hDCMem,0,0)
    SelectObject( hDCMem, hOldBmp )
    DeleteDC( hDCMem )

return nZerozeroClr

//----------------------------------------------------------------------------//

static function FontHeight( oBrw, oFont )

   local hDC
   local nHeight

   hDC := oBrw:GetDC()
   oFont:Activate( hDC )
   nHeight := GetTextHeight( oBrw:hWnd, hDC )
   oBrw:ReleaseDC()

return nHeight

//----------------------------------------------------------------------------//
static function FontEsc( oFont )

   local nEsc := 0

   if oFont != nil .and. ValType( oFont:nEscapement ) == 'N'
      nEsc = Abs( oFont:nEscapement )
   endif

return nEsc

//----------------------------------------------------------------------------//

static function HashEditBlock( oBrw, c )
return { |x| If( x == nil, oBrw:aRow[ c ], oBrw:aRow[ c ] := x ) }

//------------------------------------------------------------------//

static function ArrCalcWidths( aData, aCols )

   local aSizes
   local nRow, nCol, nRows, cType, n, uVal, aRow, nCols := 1

   if ! Empty( aCols )
      AEval( aCols, { |n| If( ValType( n ) == 'N', nCols := Max( nCols, n ), nil ) } )
   endif

   nRows       := Len( aData )
   aSizes      := Array( nCols )

   if nRows > 0
      for nRow := 1 to nRows
         aRow  := aData[ nRow ]
         if ValType( aRow ) != 'A'  // Row need not be an array
            aRow  := { aRow }
         endif
         nCols   := Len( aRow ) // ragged arrays possible
         if nCols > Len( aSizes )
            ASize( aSizes, nCols )
         endif
         nCols   := Min( Len( aRow ), nCols )
         for nCol := 1 to nCols
            uVal  := aRow[ nCol ]
            cType := ValType( uVal )
            if cType == 'C'
               if ( n := Len( Trim( uVal ) ) ) > IfNil( aSizes[ nCol ], 0 )
                  aSizes[ nCol ] := n
               endif
            else
               if ( n := Len( cValToStr( uVal ) ) ) > IfNil( aSizes[ nCol ], 0 )
                  aSizes[ nCol ] := n
               endif

            endif
         next nCol
      next nRow
   endif

   for n := 1 to Len( aSizes )
      if aSizes[ n ] == nil
         aSizes[ n ] := 10
      endif
   next n

return aSizes

//------------------------------------------------------------------//


static Function MakeColAlphabet( nCol )

   local cCol  := ""
   local nDigit

   do while nCol > 0
      nDigit   := nCol % 26
      if nDigit == 0
         nDigit   := 26
         nCol     -= 26
      endif
      cCol     := Chr( nDigit + 64 ) + cCol
      nCol     := Int( nCol / 26 )
   enddo

return cCol

//------------------------------------------------------------------//

static function MakeBlock( uVar )
return { || uVar }

//----------------------------------------------------------------------------//

//---------------------------------------------------------------------------//
// Support functions for Commands in xbrowse.ch
//----------------------------------------------------------------------------//

function XCbrowseNew( oWnd, nRow, nCol, nWidth, nHeight,;
                     aFlds, aHeaders, aColSizes,  ;
                     bChange, bLDblClick, bRClick, ;
                     oFont, oCursor, nClrFore, nClrBack, ;
                     cMsg, lUpdate, cDataSrc, bWhen, ;
                     lDesign, bValid, lPixel, nResID, lAutoSort, lAddCols, ;
                     aPics, aCols, aJust, aSort, lFooter, lFastEdit, ;
                     lCell, lLines, aRows, uBack, cBckMode, bClass, lTransparent, lNoBorder )

   local oBrw, n, i, oCol, oClass

   DEFAULT lTransparent := .F.
   DEFAULT oWnd := TWindow():New()

   // This function is intended only to support command syntax
   // and not to be used directly in application program

   if ValType( bClass ) == 'B'
      oClass      := Eval( bClass )
   endif

   if oClass != nil .and. oClass:IsKindOf( 'TXCBROWSE' )
      oBrw        := oClass:New( oWnd )
   else
      oBrw        := TXCBrows():New( oWnd ) //TXCBrwColumn
      if oClass != nil .and. oClass:IsKindOf( 'TXCBRWCOLUMN' )
         oBrw:bColClass := bClass
      endif
   endif

   oBrw:lAutoSort  := lAutoSort
   oBrw:bLDblClick := bLDblClick
   oBrw:bRClicked  := bRClick

   aFlds          := CheckArray( aFlds )
   aHeaders       := CheckArray( aHeaders )
   aColSizes      := CheckArray( aColSizes )
   aPics          := CheckArray( aPics )
   aCols          := CheckArray( aCols )
   aJust          := CheckArray( aJust )
   aSort          := CheckArray( aSort )

   if aCols != nil
      aCols          := ASize( ArrTranspose( aCols ), 6 )
   endif

   XbrwSetDataSource( oBrw, cDataSrc, lAddCols, lAutoSort, ;
      If( aCols == nil, nil, aCols[ 1 ] ), aRows, aHeaders  )

   DEFAULT aHeaders := {}, aPics := {}, aColSizes := {}, aSort := {}

   if aCols != nil
      aHeaders       := ArrMerge( aCols[ 2 ], aHeaders )
      aPics          := ArrMerge( aCols[ 3 ], aPics )
      aColSizes      := ArrMerge( aCols[ 4 ], aColSizes )
      aJust          := ArrMerge( aCols[ 5 ], aJust )
      aSort          := ArrMerge( aCols[ 6 ], aSort )
   endif

   if ! Empty( aFlds )
      for n := 1 to Len( aFlds )
         oBrw:AddCol():bEditValue   := aFlds[ n ]
      next
   endif

   for i := 1 to Len( oBrw:aCols )
      oCol  := oBrw:aCols[ i ]
      if Len( aPics ) >= i .and. aPics[ i ] != nil
         if ValType( aPics[ i ] ) == 'A'
            oCol:SetCheck( aPics[ i ] )
         elseif !Empty( aPics[ i ] )
            oCol:cEditPicture := aPics[ i ]
         endif
      endif
      if Len( aHeaders ) >= i .and. aHeaders[ i ] != nil
        oCol:cHeader   := cValToChar( aHeaders[ i ] )
      endif
      if Len( aColSizes ) >= i
         if aColSizes[ i ] != nil .and. aColSizes[ i ] < 0
            n              := -aColSizes[ i ]
            oCol:nDataLen  := Int( n )
            if n > oCol:nDataLen
               n           := Int( 10 * ( n - oCol:nDataLen ) )
               oCol:nDataDec  := n
            endif
         else
            oCol:nWidth    := aColSizes[ i ]
         endif
      endif
      if Len( aSort ) >= i .and. ! Empty( aSort[ i ] )
         oCol:cSortOrder := aSort[ i ]
      endif
   next i

   if valtype( nClrFore ) == 'N'
      DEFAULT nClrBack  := CLR_WHITE
      oBrw:bClrStd      := {|| { nClrFore, nClrBack } }
      oBrw:SetColor( nClrFore, nClrBack )
   endif

   if ValType( uBack ) $ 'ACNO'
      if ValType( uBack ) == 'A'
         n     := If( ValType( cBckMode ) == 'C', ( cBckMode != 'HORIZONTAL' ), .t. )
      else
         n     := If( ValType( cBckMode ) == 'C', AScan( { 'TILED','STRETCH','FILL' }, cBckMode ), 0 )

         n     := If( n > 0, n - 1, nil )
      endif
      oBrw:SetBackGround( uBack, n )
   endif

   oBrw:bChange       := bChange
   if oFont != nil
      oBrw:oFont      := oFont
   endif
   if bWhen != nil
      oBrw:bWhen      := bWhen
   endif
   if bValid != nil
      oBrw:bValid     := bValid
   endif
   if oCursor != nil
      oBrw:oCursor    := oCursor
   endif
   if cMsg != nil
      oBrw:cMsg       := cMsg
   endif

   oBrw:lDesign       := lDesign

   if ! Empty( aJust )
      oBrw:aJustify  := aJust
   endif

   oBrw:lFooter   := lFooter
   oBrw:lFastEdit := lFastEdit

   if lLines
      oBrw:nColDividerStyle         := LINESTYLE_BLACK
      oBrw:nRowDividerStyle         := LINESTYLE_BLACK
      oBrw:lColDividerComplete      := .T.
   endif

   if lCell
      oBrw:nMarqueeStyle            := MARQSTYLE_HIGHLCELL
   endif

   if ValType( nResID ) == 'N'
      oBrw:CreateFromResource( nResID )
   else
      if nRow != nil
         oBrw:nTop       := nRow * If( lPixel, 1, BRSE_CHARPIX_H ) // 14
      endif

      if nCol != nil
         oBrw:nLeft      := nCol * If( lPixel, 1, BRSE_CHARPIX_W )  //8
      endif

      if nWidth != nil
         if nWidth <= 0
            oBrw:nRightMargin := -nWidth
            if oWnd:IsKindOf( "TDIALOG" )
               oBrw:nRightMargin *= 2
            endif
         else
            oBrw:nRight     := oBrw:nLeft + nWidth - 1
         endif
      endif

      if nHeight != nil
         if nHeight <= 0
            oBrw:nBottomMargin    := -nHeight
            if oWnd:IsKindOf( "TDIALOG" )
               oBrw:nBottomMargin *= 2
            endif
         else
            oBrw:nBottom    := oBrw:nTop + nHeight - 1
         endif
      endif

      if lNoBorder == .t. .and. lAnd( oBrw:nStyle, WS_BORDER )
         oBrw:nStyle       -= WS_BORDER
      endif
      if lTransparent
         oBrw:lTransparent := .t.
      endif
      oBrw:lUpdate      := lUpdate
   endif

return oBrw

//--------------------------------------------------------------------------//

static function CheckArray( aArray )

   if ValType( aArray ) == 'A' .and. ;
      Len( aArray ) == 1 .and. ;
      ValType( aArray[ 1 ] ) == 'A'

      aArray   := aArray[ 1 ]
   endif

return aArray

//--------------------------------------------------------------------------//

static function ArrMerge( aArray1, aArray2 )

   local n, nLen

   if Empty( aArray1 )
      aArray1    := aArray2
   elseif ! Empty( aArray2 )
      if Len( aArray1 ) < Len( aArray2 )
         ASize( aArray1, Len( aArray2 ) )
      endif
      AEval( aArray2, { |u,i| If( u == nil, , aArray1[ i ] := u ) } )
   endif

return aArray1

//----------------------------------------------------------------------------//

Function DrawRowN( oCol, hDC, cText, aCoors, oFont, aItems, nH, nW )

   local hOldFont
   DEFAULT nH   := 30
   DEFAULT nW   := 0

   DrawText( hDC, cText, aCoors )
   if !empty( aItems )
   aCoors[ 1 ] += nH
   aCoors[ 2 ] += nW
   hOldFont = SelectObject( hDC, oFont:hFont )
   DrawText( hDC, aItems[ oCol:oBrw:KeyNo ], aCoors )
   SelectObject( hDC, hOldFont )
   endif
return nil

//----------------------------------------------------------------------------//

//----------------------------------------------------------------------------//

function TXCBrows( bChild )

   if bXBrowse == nil
      bXBrowse    := { || TXCBrowse() }
   endif

   if ValType( bChild ) == 'B' .and. ; // retained for backward compatibility
      ValType( Eval( bChild ) ) == 'O' .and. ;
      Eval( bChild ):IsKindOf( TXCBrowse() )

      bXBrowse    := bChild
   endif

return Eval( bXBrowse )

//----------------------------------------------------------------------------//

Function xBrwTextFile( cFileTxt, cTitle, lLines, lFmto )
  local oDlg, oBrw, oCol, oSay, oBtn
  local oFont
  local lExit:= .T.
  local oTxt := TTxtFile():New( cFileTxt )
  local hBmpRec
  local aBmpPal  := {}
  local nC       := 0
  local aLines   := {"Fichero de Texto"}
  local aGrad    := {  {  1/20, RGB( 235, 244, 253 ), RGB( 235, 244, 253 ) }, ;
                    {  1/20, RGB( 125, 162, 206 ), RGB( 125, 162, 206 ) }, ;
                    { 16/20, RGB( 220, 235, 252 ), RGB( 193, 219, 252 ) }, ;
                    {  1/20, RGB( 125, 162, 206 ), RGB( 125, 162, 206 ) }, ;
                    {  1/20, RGB( 235, 244, 253 ), RGB( 235, 244, 253 ) }  }


  DEFAULT cTitle := "Archivo de Texto"
  DEFAULT lLines := .F.
  DEFAULT lFmto  := .F.
                                   // 08, 15
  DEFINE FONT oFont NAME "Arial" SIZE 08, 16// BOLD

  aBmpPal   := PalBmpRead( ,  ".\res\gl216rnw.bmp" )
  hBmpRec   := aBmpPal[ 1 ]
  //hPalette := aBmpPal[ 2 ]

  DEFINE DIALOG oDlg TITLE cTitle SIZE 1200, 650 PIXEL TRANSPARENT FONT oFont
  //NewDlg( lMax , lMdi, oParent, nTop, nLeft, nWidth, nHeight, nColor )
  //oDlg := TDialogUI():NewDlg( .T. , .F., , 100, 100, 1200, 658,  )
  //oDlg:lBttIconUI     := .F.

  if !lFmto
     if !lLines
        //aLines := DaFmtoTxt( oBrw, oDlg, oTxt )
        @ 0,0 XCBROWSE oBrw OF oDlg ; //:oWndUI;
          COLUMNS oTxt:cLine ; //"cLine" ; //"No","cLine";
          OBJECT oTxt;
          FONT oFont ;
          NOBORDER ;
          PIXEL ;
          SIZE 550, 285    //530, 285
     else
        @ 0,0 XCBROWSE oBrw OF oDlg ; //:oWndUI;
          COLUMNS "No","cLine" ;
          OBJECT oTxt ;
          FONT oFont  ;
          NOBORDER ;
          PIXEL ;
          SIZE 550, 285    //530, 285

          WITH OBJECT oBrw:No
               :nWidth    := 50
               :bClrStd := {|| { METRO_WINDOW, METRO_GRIS2 } }
               :bStrData  := { || StrZero( oBrw:KeyNo(), 4 ) }
          ENDWITH
     endif
  else
//     aLines := DaFmtoTxt( oBrw, oDlg, oTxt )
     @ 0,0 XCBROWSE oBrw OF oDlg ; //:oWndUI;  //COLUMNS "cLine" ; //"No","cLine";
           ARRAY aLines ;
           FONT oFont ;
           NOBORDER ;
           PIXEL ;
           SIZE 550, 285    //530, 285
     if lLines
        //ADD TO oBrw AT 1 DATA STRZERO( oBrw:KeyNo(), 4 ) HEADER 'No' ;
        //PICTURE '9999' WIDTH 40
        WITH OBJECT oBrw:No
             :nWidth    := 50
             :bClrStd := {|| { METRO_WINDOW, METRO_GRIS2 } }
             :bStrData  := { || StrZero( oBrw:KeyNo(), 4 ) }
        ENDWITH
     endif
  endif

  WITH OBJECT oBrw
    //:lVScroll         := .T.
    :lHScroll         := .F.
    :lHeader          := .F.
    :lFooter          := .F.
    :nFooterHeight    := 0
    :lRecordSelector  := .T.
    :lLinDataSep      := .F.
    :nRecSelColor     := METRO_GRIS2
    :hBmpRecSel       := hBmpRec    //FwRArrow()
    :nRecSelWidth     := 40
    :nSpRecordSel     := 12

    :nFreeze          := 1
    //:bClrSelFocus := {|| { CLR_BLACK, aGrad } }
    :bClrSelFocus    := {|| if( oBrw:nColSel = 1, { METRO_GRIS2, METRO_WINDOW },;
                         { , METRO_GRIS8 } ) }
    :nMarqueeStyle   := 0   //3
    :nStretchCol     := STRETCHCOL_LAST
    :nRowHeight      := 20

    :lKinetic        := .T.
  END

  if Len( oBrw:aCols ) > 0
     WITH OBJECT oBrw:aCols[ Len( oBrw:aCols ) ]    //oBrw:cLine

          //:bFooter:= {|| "Linea = " + LTrim( Str( oTxt:RecNo() ) ) + " / " + LTrim( Str( oTxt:RecCount() ) )}
          //:cHeader:= cTitle
          /*
          nC := oBrw:cLine:nWidthChr()

          :bStrData := { || nC := 60, ; //oBrw:cLine:nWidthChr(),;
                            if( len( oTxt:cLine ) <= nC, oTxt:cLine , ;
                            (Left( oTxt:cLine, nC )+CRLF+;
                            Substr( oTxt:cLine, nC+1, Len( oTxt:cLine )) ) ;
                            ) }
          */
          :bClrStd := {|| if ( Left( oTxt:cLine , 1 ) = " ", ;
                             { CLR_RED, CLR_WHITE }, { CLR_BLUE, CLR_WHITE } )}
     END
  endif

  oBrw:CreateFromCode()

  //lFoco    := .F.
  //oBrw:bLostFocus := { || if( lFoco, oDlg:oWndUI:End(), ) }
  //oBrw:bGotFocus  := { || lFoco := .T. }

  //oDlg:oWndUI:bInit := { || if( lFmto, ;
  oDlg:bInit := { || if( lFmto, ;
      ( oBrw:SetArray( DaFmtoTxt( oBrw, oDlg, oTxt ) ), oBrw:Refresh() ), ) }

  //oDlg:oWndUI:bStart := { || if( lFmto, ;
  //                            ( oBrw:SetArray( DaFmtoTxt( oBrw, oDlg, oTxt ) ), oBrw:Refresh() ), ) }

  //oDlg:oWndUI:oClient   := oBrw

  //oDlg:ActivaDlgUI( , .T. )    //ActivaDlgUI( lParent, lModal )

  ACTIVATE DIALOG oDlg CENTERED NOMODAL //;
  /*
  ON CLICK If( nRow < 0 .or. nCol < 0 .or. ;
                   nRow > oDlg:oWndUI:nHeight - 30 .or. nCol > oDlg:oWndUI:nWidth,;
                   oDlg:End(),)
  //  VALID lExit;
  //  CENTERED
  */
  // Clierra el Archivo Abierto
  IF !FClose( oTxt:hFile )
     MsgAlert( "Error Cerrando Archivo " + cFileTxt )
  ENDIF

  oFont:End()
  aBmpPal    := Nil
  hBmpRec    := Nil

RETURN nil

//----------------------------------------------------------------------------//
// Funcion para cerrar el dialogo si se pulsa fuera del control
//  oDlg:oWndUI:bLClicked := { |nRow,nCol,nKF| ClickOutEnd( nRow,nCol,nKF,oDlg:oWndUI ) }  //oDlg:End() }

//  oDlg:oWndUI:bStart    := { || SetCapture( oDlg:oWndUI:hWnd ) }
//----------------------------------------------------------------------------//
Function ClickOutEnd( nRow,nCol,nKF,oDlg )

 If nRow < 0 .or. nCol < 0 .or. ;
    nRow > oDlg:nBottom .or. nCol > oDlg:nRight //oDlg:nWidth
    //nRow > oDlg:nHeight - 30 .or. nCol > oDlg:nWidth
    oDlg:End()
 else

 endif

Return nil

//----------------------------------------------------------------------------//

Function DaFmtoTxt( oBrw, oDlg, oTxt )
Local nAnchoCol   := oBrw:aCols[ Len( oBrw:aCols ) ]:nWidth
Local nAnchoFont  := oBrw:oFont:nWidth
Local nNumChar    := Int( nAnchoCol / nAnchoFont )
Local nLenFich    := oTxt:nLen
Local nTotLines   := oTxt:nTLines
Local nL          := 0
Local aLines      := {}
Local x           := 0
Local y           := 0
Local cCharLine   := ""
Local cLine       := ""
Local nSpace      := 0
/*
  Str(oBrw:cLine:nWidthChr())  + CRLF + ;
  Str(GetTextWidth( oBrw:hDC, oTxt:cLine, oBrw:oFont:hFont )) + CRLF +;
*/
? nAnchoCol, nAnchoFont, nNumChar, nTotLines
For nL = 1 To nTotLines
    x  := 0
    y  := 0
    cCharLine   := ""
    cLine       := ""
    cLine       := oTxt:ReadLine()
    AAdd( aLines, cLine )
    if Len( cLine ) > nNumChar
       nSpace := Len( cLine ) - Len( LTrim( cLine ) )

       Do While Len( cLine ) >= nNumChar  //x < Int( Len( aLines[ Len( aLines ) ] ) / nNumChar )

          if Substr( cLine, nNumChar, 1 ) = " "

             cCharLine := Left( cLine, nNumChar )

          else

             y := 0
             Do While .t.
                y++
                if Len( cLine ) >= nNumChar - y
                   if Substr( cLine, nNumChar - y, 1 ) = " "
                      cCharLine := Left( cLine, nNumChar - y )
                      Exit
                   endif
                else
                   cCharLine := cLine
                   Exit
                endif
             Enddo

          endif

          if x = 0
             aLines[ Len( aLines ) ] := cCharLine
          else
             AAdd( aLines, cCharLine )
          endif

          if Len( cLine ) - ( nNumChar - y ) > 0
             cLine := Space(nSpace)+Right( cLine, Len( cLine ) - ( nNumChar - y ) )
          else
             AAdd( aLines, Space(nSpace)+cLine )
             Exit
          endif

          x++
       Enddo
    endif
    oTxt:Skip()
Next nL
oTxt:GoTop()
? Len( aLines )
Return aLines

//----------------------------------------------------------------------------//
// XBrowse de creacion de Lateral
//----------------------------------------------------------------------------//

Function XCBrowseMnu( oWnd, oFont, nT, nL, nW, nH, aItems, aItems1, bActMnu, lC)
Local oBrw
Local oFont2
Local oFont3
Local nHF2
Local nHF3
Local nHF1
Local hBmpRec
Local nAltoFila
Local aBmpPal  := {}

DEFAULT bActMnu := { || .T. }
DEFAULT lC      := .T.

  if lC
     aBmpPal   := PalBmpRead( ,  ".\res\gl216rnb.bmp" )
     hBmpRec   := aBmpPal[ 1 ]
  endif

  DEFINE FONT oFont2 NAME "Segoe UI" SIZE 0, -18 //-24  //-32
  DEFINE FONT oFont3 NAME "Segoe UI Light BOLD" SIZE 0, -11 //-12 //-10 //-16
  nHF2   := -( oFont2:nInpHeight ) - 2
  nHF3   := -( oFont3:nInpHeight ) - 2

  nHF1   := Int( ( nHF2 ) * 2 )*CalculaRes(1) //- 4
  nAltoFila := (( nHF2 + nHF3 ) * 2 + 4)*CalculaRes(1)
       //12                                  //310
  @ 140, 8 XCBROWSE oBrw ARRAY aItems COLSIZES 330 CELL ;
      FONT oFont2 SIZE 210, nAltoFila * Len( aiTems )+2 PIXEL NOBORDER OF oWnd
           //200 //315    , 620

  WITH OBJECT oBrw

       :nDataLines           := 2
       :nRowHeight           := nAltoFila //50 //56 //60 //90
       if lC
          :lRecordSelector      := .T.
          :nRecSelColor         := METRO_WINDOW
          :hBmpRecSel           := hBmpRec    //FwRArrow()
       else
          :lRecordSelector      := .F.
       endif
       //:nRecSelWidth       := 40

       if Upper( oBrw:ClassName() ) == "TXCBROWSE"
          :lLinDataSep         := .F.
          :nSpRecordSel         := 6
       endif
       :lColDividerComplete  := .F.

       :lHeader   = .F.
       :lHScroll  = .F.
       :lVScroll  = .F.
       :nStretchCol = 1
       :bClrStd = { || { METRO_AZUL3, METRO_WINDOW } }
       :bClrSel = { || { METRO_WINDOW, METRO_GRAYTEXT } }
       :bClrSelFocus = { || { METRO_AZUL3, METRO_AZUL14 }} //METRO_WINDOW, METRO_AZUL13 } }  //METRO_VERDE13
       :SetArray( aItems )
       :aCols[1]:bLDClickData   := bActMnu
       :bKeyChar   := :aCols[1]:bLDClickData
       :CreateFromCode()
       :aCols[ 1 ]:bPaintText = { | oCol,hDC,cText,aCoors,aColors,lHighlight |;
             DrawRowN( oCol, hDC, cText, aCoors , oFont3, aItems1, nHF1 )}
  END

  aBmpPal    := Nil
  hBmpRec    := Nil

  oFont2:End()
      //oFont3:End()
  oFont2     := Nil
      //oFont3     := Nil

Return oBrw

//----------------------------------------------------------------------------//

Static Function CalculaRes( nTp )  // nTp -> 0 Ancho   nTp -> 1 Alto
local nAncho
local nAlto
local nPorcAnc
local nPorcAlt
DEFAULT nTp   := 0
      nAncho  := GetSysMetrics( 0 ) //ScreenWidth()  //GetSysmetrics( 4 )
      nAlto   := GetSysMetrics( 1 ) //ScreenHeight() //GetSysmetrics( 3 )
      if Empty( nTp )
         nPorcAnc := Round( ( nAncho/1366 ) , 4 )
      else
         nPorcAlt := Round( ( nAlto/768 ) , 4 )
      endif
     //    1920    1058      1920            1080               17                  17            22
     // ? nAncho, nAlto , GetSysmetrics(0), GetSysmetrics(1), GetSysmetrics(2),GetSysmetrics(3),GetSysmetrics(4)

Return IF( Empty( nTp ), nPorcAnc, nPorcAlt )


//----------------------------------------------------------------------------//
// Funciones para crear el Browse de Ficheros y Carpetas
// Basado en el codigo de XBrwDisk.Prg
//----------------------------------------------------------------------------//

Function XCBrowseTree( oDlg, oFont, nT, nL, nW, nH, cXPath, cTit, nTip, lTree, cExt )
Local oBrw
Local oTree
Local bDLClick
Local aDir       := {}
Local x
Local y
Local z
Local n
//Local cExt       := ""
Local aFichs     := {}
Local cFolder    := ""
Local oFont3
Local oFont2
DEFAULT nT       := 0
DEFAULT nL       := 0
DEFAULT nW       := 300
DEFAULT nH       := 200
DEFAULT cXPath   := ""
DEFAULT cTit     := ""
DEFAULT nTip     := 0
DEFAULT lTree    := .T.
DEFAULT cExt     := "*.*"

   //cTit := ""
   nTip := 1


   DEFINE FONT oFont2 NAME "Segoe UI BOLD" SIZE 0, -14
   DEFINE FONT oFont3 NAME "Segoe UI" SIZE 0, -10  //-12

   if empty( cXPath )
      cXPath  := HB_Curdrive() + ":\" + CurDir()   //+ "\" + cExt
   else
      cXPath  := RTrim( cXPath )
      cXPath  := if( Right( cXPath, 1 ) = "\", Left( cXPath, Len( cXPath ) -1),;
                     cXPath )
   endif

   aDir   := Directory( RTrim(cXPath) + "\" + cExt, 'D' )
   aDir   := ASort( aDir ,,, { |x,y| if( x[2] <= y[2], ;
                                    if( "D" $ x[5], .T., .F. ), .F. ) } )
   if lTree

   cTit   := ""
   aFichs := XBrwLinDet( aDir, nTip )

   oTree  := XMakeTree( cXPath )

   else
      //aFichs := Array( Len( aDir ) )
      //AEVal( aDir, { | aD | aFichs := aD[1] } , , )
      For n = 1 to Len( aDir )
          AAdd( aFichs, Upper( aDir[ n ][ 1 ] ) )
      Next n

   endif

   @ nT, nL XBROWSE oBrw SIZE nW, nH FONT oFont2 CELL PIXEL OF oDlg NOBORDER

   if lTree
   oBrw:SetTree( oTree, { ".\res\open1n24.bmp", ;
                          ".\res\folder24.bmp", ;
                          ".\res\source24.bmp" } )

   oBrw:bKeyChar  := { |nKey| If( nKey == VK_RETURN .and. ;
                        !Empty( oBrw:oTreeItem:bAction ), ;
                        Eval( oBrw:oTreeItem:bAction, oBrw:oTreeItem ), nil ) }
   else
      oBrw:SetArray( aFichs )
      oBrw:AddCol()
   endif

   if empty( cTit )
      oBrw:lHeader       := .F.
   else
      oBrw:lHeader       := .T.
      oBrw:lAutoSort     := .F.
   endif
   oBrw:lHScroll         := .F.
   oBrw:lVScroll         := .T.
   //oBrw:nStretchCol := 1

   oBrw:nDataLines           := if( lTree, 2, 1 )
   oBrw:nRowHeight           := if( lTree, 39, 24 )

   oBrw:nRecSelColor     := METRO_WINDOW
   oBrw:lRecordSelector  := .F.
   oBrw:bClrStd          := { || { METRO_AZUL3, METRO_WINDOW } }
   oBrw:bClrSel          := { || { METRO_WINDOW, METRO_GRAYTEXT } }
   oBrw:bClrSelFocus     := { || { METRO_WINDOW, METRO_AZUL13 } }  //METRO_VERDE13


   WITH OBJECT oBrw:aCols[ 1 ]

      //:AddBmpFile( ".\16x16\hdrive.bmp" )
      :nWidth     := oBrw:nWidth - if( oBrw:lVScroll, 20 , 0 )  //200
      if !empty( cTit )

         :cHeader      := cTit
      endif

      if lTree
      bDLClick    := :bLDClickData
      :bLDClickData  := { |r,c,f,o| XToggleFolder( r,c,f,o,bDLClick ) }

      :bBmpData   := { || If( ':' $ oBrw:oTreeItem:cPrompt, 4, ;
                          If( 'D' $ oBrw:oTreeItem:Cargo[ 5 ], ;
                          If( oBrw:oTreeItem:lOpened, 1, 2 ), 3 ) ) }
      endif

   END

   /*
   ADD TO oBrw DATA oBrw:oTreeItem:Cargo[ 2 ] ;
         PICTURE '@EZ 999,999,999' HEADER 'Bytes'
   ADD TO oBrw DATA oBrw:oTreeItem:Cargo[ 3 ] HEADER 'Date'
   ADD TO oBrw DATA oBrw:oTreeItem:Cargo[ 4 ] HEADER 'Time'
   ADD TO oBrw DATA oBrw:oTreeItem:Cargo[ 5 ] HEADER 'Attr'
   */

   oBrw:CreateFromCode()

   if lTree
   oBrw:aCols[ 1 ]:bPaintText = { | oCol,hDC,cText,aCoors,aColors,lHighlight |;
             DrawRowN( oCol, hDC, cText, aCoors , oFont3, aFichs, 18, ) }
   endif

   oFont2:End()
   oFont2  := Nil

Return oBrw

//----------------------------------------------------------------------------//

Function XToggleFolder( r, c, f, oCol, bBlock )

   local oBrw  := oCol:oBrw
   local oItem := oBrw:oTreeItem

   If ! oItem:lOpened .and. ! Empty( oItem:bAction )
      Eval( oItem:bAction, oItem )
   endif

   if bBlock != nil
      Eval( bBlock, r, c, f, oCol )
   endif

return nil

//----------------------------------------------------------------------------//

Function XMakeTree( cXPath )

   local oTree
   local oItem
   local n
   local aDrives  := {}

   DEFAULT cXPath := ""  //CurDir()

if empty( cXPath )
   aDrives  := aDrives( 2 )     // Hard disks
   TREE oTree
   For n := 1 to Len( aDrives )
      TREEITEM oItem PROMPT aDrives[ n ]
      oItem:Cargo    := { aDrives[ n ], 0, CtoD( '' ), Space( 8 ), 'D', ;
                        aDrives[ n ] }
      oItem:bAction  := { |o| o:SetTree( XSubTree( o ) ), o:bACtion := nil }
   Next n
   ENDTREE

else

   TREE oTree
      TREEITEM oItem PROMPT cXPath
      oItem:Cargo    := { cXPath, 0, CtoD( '' ), Space( 8 ), 'D', cXPath }
      oItem:SetTree( XSubTree( oItem ) )
      oItem:Open()
   ENDTREE

endif
return oTree

//----------------------------------------------------------------------------//

Function XSubTree( oParent, bAct )

   local oTree
   local n
   local oItem
   local nLevel
   local nItems  := 0
   local cFolder := ""
   local aDir    := {}
   local x
   local y
   local z

   DEFAULT bAct  := { |o| MsgInfo( o:cPrompt ) }

   cFolder := if( Rat( "\",;
         RTrim(oParent:Cargo[ 6 ]) ) < Len( RTrim( oParent:Cargo[ 6 ] ) ),;
         RTrim(oParent:Cargo[ 6 ]) ,;
         Left( RTrim(oParent:Cargo[ 6 ]), Len(RTrim(oParent:Cargo[ 6 ]))-1) )

   oParent:Cargo[ 6 ]   := cFolder
   aDir                 := Directory( cFolder + '\*.*', 'D' )
   aDir  := ASort( aDir ,,, { |x,y| if( x[2] <= y[2], if( "D" $ x[5], .T., .F. ), .F. ) } )

   nLevel   := oParent:nLevel + 1

   TREE oTree
   for n := 1 to Len( aDir )
      if ! ( aDir[ n ][ 1 ] = '.' )

         TREEITEM oItem PROMPT aDir[ n ][ 1 ]

         oItem:nlevel := nLevel
         oItem:Cargo  := aDir[ n ]
         AAdd( oItem:Cargo, cFolder + Chr(92) + aDir[ n ][ 1 ] )

         if 'D' $ aDir[ n ][ 5 ]
            oItem:bAction  := { |o| o:SetTree( XSubTree( o ) ), o:bACtion := nil }
         else
            oItem:bAction  := bAct
         endif
         nItems++
      endif
   next
   if nItems == 0
      n--
      TREEITEM oItem PROMPT ''
      oItem:nlevel := nLevel
      aDir[ n ][ 5 ] := 'A'
      oItem:Cargo  := { '', 0, CToD( '' ), Space(8), ' ', '' }
      AAdd( oItem:Cargo, cFolder + Chr(92) + aDir[ n ][ 1 ] )
   endif
   ENDTREE

return oTree

//----------------------------------------------------------------------------//

Function XBrwLinDet( aDir, nTip )
Local x
Local y
Local z
Local cExt       := ""
Local aFichs     := {}

   For z = 1 to Len( aDir )
     cExt       := ""
     if z > 1
      if "D" $ aDir[ z, 5 ]
         AAdd( aFichs, "Carpeta de Archivos" )
      else
       if empty( nTip )
         cExt := Upper( Right( aDir[ z, 1 ], Len(aDir[ z, 1 ])-RAt(".",aDir[ z, 1 ]) ) )
         Do Case
            Case cExt = "BAK"
                AAdd( aFichs, "Archivo Copia Backup" )
            Case cExt = "BMP"
                AAdd( aFichs, "Imagen BitMap" )
            Case cExt = "C"
                AAdd( aFichs, "Codigo Fuente C/C++" )
            Case cExt = "CH"
                AAdd( aFichs, "Cabecera FW / Harbour" )
            Case cExt = "EXE"
                AAdd( aFichs, "Aplicacion Ejecutable" )
            Case cExt = "H"
                AAdd( aFichs, "Cabecera C / C++" )
            Case cExt = "ICO"
                AAdd( aFichs, "Imagen Icono" )
            Case cExt = "INI"
                AAdd( aFichs, "Fichero Configuracion" )
            Case cExt = "LIB"
                AAdd( aFichs, "Libreria de Fuentes" )
            Case cExt = "LOG"
                AAdd( aFichs, "Archivo Informacion" )
            Case cExt = "MAK"
                AAdd( aFichs, "Proyecto Make" )
            Case cExt = "OBJ"
                AAdd( aFichs, "Fichero Compilado" )
            Case cExt = "PNG"
                AAdd( aFichs, "Imagen Png" )
            Case cExt = "PRG"
                AAdd( aFichs, "Codigo Fuente FW/Harbour" )
            Case cExt = "PVK"
                AAdd( aFichs, "Plantilla Visual Make" )
            Case cExt = "TXT"
                AAdd( aFichs, "Documento de Texto" )
            Case cExt = "VMH"
                AAdd( aFichs, "Proyecto Visual Make" )
            Case cExt = "VRC"
                AAdd( aFichs, "Proyecto Verce" )
            Case cExt = "ZIP"
                AAdd( aFichs, "Carpeta Comprimida" )
            Otherwise
                AAdd( aFichs, "Archivo" )
         EndCase
       else
         AAdd( aFichs, Dtoc( aDir[z, 3] )+"  "+aDir[z, 4]+" "+Str( aDir[z, 2], 8, 0 ) )
       endif
      endif
     else
      //AAdd( aFichs, "Carpeta Seleccionada" )
     endif
   Next z

Return aFichs
//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//


//----------------------------------------------------------------------------//


